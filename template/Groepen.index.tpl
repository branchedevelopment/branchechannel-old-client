{include file="Main.header.tpl"}

{if $Session->data !== false}

		<div class="title">
			<h1>Groepen overzicht</h1>
			<span class="options">Totaal: {$Page->groepen|count}</span>
			<div style="clear: both;"></div>
		</div>
		
		<a href="groepen/nieuw/"><img src="template/images/icons/add.png" alt="" /> Groep toevoegen</a>
		
		<table style="margin-top: 20px;" class="overview">
			<tr style="border: none;">
				<th width="50%">Groepsnaam</th>
				<th width="20%">Aangesloten abonnementen</th>
				<th width="20%">Aangesloten klanten</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
{foreach $Page->groepen as $item}
			<tr>
				<td><a href="groepen/{$item.id}/bekijk/">{$item.company}</td></td>
				<td>{$item.subscriptions}</td>
				<td>{$item.customers}</td>
				<td style="text-align: right;">
					<a href="groepen/{$item.id}/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
					<a href="groepen/{$item.id}/status/"><img src="template/images/icons/{if $item.status}accept{else}delete{/if}.png" alt="" title="{if $item.status}Ina{else}A{/if}ctief maken" /></a> 
					<a href="groepen/{$item.id}/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
				</td>
			</tr>
{/foreach}
			<tr>
				<td colspan="2"><!--Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina--></td>
				<td colspan="2"><!--1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a>--></td>
			</tr>
		</table>
		
{/if}
		
{include file="Main.footer.tpl"}