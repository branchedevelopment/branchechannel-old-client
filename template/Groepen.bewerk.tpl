{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Groep: {$Page->groep.company}</h1>
		{if $Page->groep.logo}
		<img src="http://content.branchechannel.com/groups/{$Page->groep.id}.png" alt="" />
		{/if}
		
		<div class="title">
			<h2>Gegevens bewerken</h2> 
		</div>
		<form method="post" enctype="multipart/form-data">
			<table>
				<tr>
					<th style="width: 20%;">Groepsnaam</th>
					<td><input type="text" name="company" value="{$Page->groep.company}" /></th>
				</tr>
				<tr>
					<th>Contactpersoon</th>
					<td><input type="text" name="name" value="{$Page->groep.name}" /> <input type="radio" name="title" value="de heer" id="heer"{if $Page->groep.title == 'de heer'} checked="checked"{/if} /> <label for="heer">de heer</label> &nbsp; <input type="radio" name="title" value="mevrouw" id="mevrouw"{if $Page->groep.title == 'mevrouw'} checked="checked"{/if} /> <label for="mevrouw">mevrouw</label></td>
				</tr>
				<tr>
					<th>E-mailadres</th>
					<td><input type="text" name="emailaddress" value="{$Page->groep.emailaddress}" /></td>
				</tr>
				<tr>
				    <th>Wachtwoord</th>
				    <td><input type="password" name="wachtwoord" /></td>
				</tr>
				<tr>
				    <th>Logo</th>
				    <td><input type="file" name="logo" /></td>
				</tr>
                <tr>
                    <th>Achtergrondkleur</th>
                    <td><input type="text" name="achtergrondkleur" id="achtergrondkleur" class="kleur" value="{$Page->groep.achtergrondkleur}" style="background-color: {$Page->groep.achtergrondkleur}; color: {$Page->groep.tekstkleur};" readonly /></td>
                </tr>
                <tr>
                    <th>Tekstkleur</th>
                    <td><input type="text" name="tekstkleur" id="tekstkleur" class="kleur" value="{$Page->groep.tekstkleur}" style="background-color: {$Page->groep.achtergrondkleur}; color: {$Page->groep.tekstkleur}" readonly /></td>
                </tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<th></th>
					<td><input type="submit" name="edit" value="Wijzigingen opslaan" /> &nbsp; <input type="button" value="Annuleren" onclick="history.go(-1)" /></td>
					<th></th>
					<td></td>
				</tr>
			</table>
		</form>
{/if}
		
{include file="Main.footer.tpl"}