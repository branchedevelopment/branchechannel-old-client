<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Branche Channel</title>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9pt;
	color: #000;
	width: 100%;
	height: 100%;
}

.inhoud {
	clear: both;
	margin: 140px 20px 20px 20px;
}

table {
	clear: both;
	width: 100%;
	font-size: 9pt;
	color: #000;
}

table, th, td, tr {
	border: 0;
	border-collapse: collapse;
	vertical-align: top;
}

th {
	text-align: left;
}

td {
	padding: 5px 0px;
	color: #000;
}

h1 {
	margin: 0 0 5px 0;
	padding: 0;
	font-size: 16pt;
	font-weight: normal;
}

h2 {
	margin: 20px 0 5px 0;
	padding: 0;
	font-size: 12pt;
	font-weight: normal;
}

p {
	margin: 0 0 10px 0;
	line-height: 12pt;
}

</style>
</head>
<body>
<div class="inhoud">
	<table>
		<tr>
			<td colspan="4">
				<div style="padding: 30px 0px 0px 30px">
					{$klant.company}<br =/>
					T.a.v. {$klant.title} {$klant.name}<br />
					{$klant.street} {$klant.number}<br />
					{$klant.zipcode} {$klant.city}
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px; text-align: right;">Almelo, {$smarty.now|date_format:'%e %B %Y'}</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px;">
				<h1>Hartelijk welkom bij {$klant.groupname}TV</h1>
				<p>Geachte {$klant.title} {$klant.name}</p>
				
				<p>Een aantal dagen geleden heeft u zich geregistreerd voor {$klant.groupname}TV en wij willen u ook namens {$klant.groupname} hartelijk welkom heten bij Branche Channel. Branche Channel levert het ChannelCMS, het online beheersysteem, waarmee u uw eigen {$klant.groupname}TV kunt onderhouden.</p>
				
				<p>Wilt u meer uitleg over ons ChannelCMS dan verwijzen wij u graag naar onze instructievideo's welke u online kunt bekijken op <b>http://{$klant.groupname|lower}.branchechannel.com/tutorials</b>.</p>
				
				<h2>Uw inloggegevens</h2>
				<p>Om in te loggen in uw persoonlijke omgeving in het ChannelCMS gaat u naar <b>http://admin.branchechannel.com</b> en logt u in met de volgende gegevens:</p>
			</td>
		</tr>
		<tr>
			<td colspan="1">E-mailadres</td>
			<td colspan="3">{$klant.emailaddress}</td>
		</tr>
		<tr>
			<td colspan="1">Wachtwoord</td>
			<td colspan="3"><i>Het wachtwoord heeft u reeds ontvangen in de bevestigings e-mail</i></td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Heeft u de Branche Channel Player nog niet besteld?</h2>
				<p>Deze player kunt u online bestellen bij onze hardware-partner Multimediacenter via de webshop <b>http://branchechannel.multimediacenter.nl</b>. Deze kunt u gemakkelijk en veilig betalen met iDeal.</p>
			
			
				<h2>Machtiging automatische incasso</h2>
				<p>Tevens treft u het machtigingsformulier aan voor een doorlopende automatische incasso voor de maandelijkse abonnementskosten welke elke kwartaal worden ge&iuml;nd. Wij verzoeken u vriendelijk dit formulier verder in te vullen en deze ondertekend aan ons te retourneren middels bijgaande retourenveloppe. U hoeft deze niet te frankeren.</p>
			
				<p>Namens {$klant.groupname} en Branche Channel wensen wij u veel plezier en commercieel succes met {$klant.groupname}TV.</p>
				
				<p style="padding-top: 10px;">Met vriendelijke groet,</p>
				
				<p><img src="http://backend.branchechannel.com/template/images/factuur_signature.jpg" alt="" /></p>
				
				<p>Team Branche Channel</p>
				<p style="font-size: 8pt;">Bijlage: machtigingsformulier doorlopende automatische incasso &amp; retourenveloppe</p>
			</td>
		</tr>
	</table>
</div>

<div style="page-break-after: always"></div>

<div class="inhoud">
	<table>
		<tr>
			<td colspan="4">
				<div style="padding: 30px 0px 0px 30px">
					Branche Channel BV<br />
					T.a.v. de administratie<br />
					Mooie Vrouwenweg 67<br />
					7608 RC Almelo
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px; text-align: right;">Almelo, {$smarty.now|date_format:'%e %B %Y'}</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px;">
				<h1>Machtiging</h1>
				<p><b>Doorlopende automatische incasso</b></p>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Gegevens opdrachtgever</h2>
			</td>
		</tr>
		<tr>
			<td colspan="1">Bedrijf</td>
			<td colspan="3">{$klant.company}</td>
		</tr>
		<tr>
			<td colspan="1">Naam</td>
			<td colspan="3">{$klant.name}</td>
		</tr>
		<tr>
			<td colspan="1">Adres</td>
			<td colspan="3">{$klant.street} {$klant.number}</td>
		</tr>
		<tr>
			<td colspan="1">Postcode en plaats</td>
			<td colspan="3">{$klant.zipcode} {$klant.city}</td>
		</tr>
		<tr>
			<td colspan="1">Rekeningnummer</td>
			<td colspan="3">{$klant.bank}</td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Gegevens incassant</h2>
			</td>
		</tr>
		<tr>
			<td colspan="1">Naam</td>
			<td colspan="3">Branche Channel BV</td>
		</tr>
		<tr>
			<td colspan="1">Adres</td>
			<td colspan="3">Mooie Vrouwenweg 67</td>
		</tr>
		<tr>
			<td colspan="1">Postcode en plaats</td>
			<td colspan="3">7608 RC Almelo</td>
		</tr>
		<tr>
			<td colspan="1">Bedrag</td>
			<td colspan="3">&euro; {$klant.subscriptionprice} incl. BTW</td>
		</tr>
		<tr>
			<td colspan="1">Reden afschrijving</td>
			<td colspan="3">Abonnementskosten {$klant.groupname}TV {$klant.subscriptionname}</td>
		</tr>
		<tr>
			<td colspan="1">Datum afschrijving</td>
			<td colspan="3">Op de eerste dag van ieder volgend kwartaal</td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Ondertekening opdrachtgever</h2>
			</td>
		</tr>
		<tr>
			<td colspan="1">Datum</td>
			<td colspan="3">______________________________________</td>
		</tr>
		<tr>
			<td colspan="1">Plaats</td>
			<td colspan="3">______________________________________</td>
		</tr>
		<tr>
			<td colspan="1" style="padding-top: 30px;">Handtekening</td>
			<td colspan="3" style="padding-top: 30px;">______________________________________</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 20px;">
				<p>Met het ondertekenen van dit machtigingformulier geeft de opdrachtgever toestemming aan 
				Branchel Channel BV om doorlopend bovengenoemd bedrag van zijn/haar rekening te laten afschrijven.</p>
			</td>
		</tr>
	</table>
</div>

<div style="page-break-after: always"></div>

<div class="inhoud">
	<table>
		<tr>
			<td colspan="4">
				<div style="padding: 30px 0px 0px 30px">
					{$klant.company}<br =/>
					T.a.v. {$klant.title} {$klant.name}<br />
					{$klant.street} {$klant.number}<br />
					{$klant.zipcode} {$klant.city}
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px; text-align: right;">Almelo, {$smarty.now|date_format:'%e %B %Y'}</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px;">
				<h1>Hartelijk welkom bij {$klant.groupname}TV</h1>
				<p>Geachte {$klant.title} {$klant.name}</p>
				
				<p>Een aantal dagen geleden heeft u zich geregistreerd voor {$klant.groupname}TV en wij willen u ook namens {$klant.groupname} hartelijk welkom heten bij Branche Channel. Branche Channel levert het ChannelCMS, het online beheersysteem, waarmee u uw eigen {$klant.groupname}TV kunt onderhouden.</p>
				
				<p>Wilt u meer uitleg over ons ChannelCMS dan verwijzen wij u graag naar onze instructievideo's welke u online kunt bekijken op <b>http://{$klant.groupname|lower}.branchechannel.com/tutorials</b>.</p>
				
				<h2>Uw inloggegevens</h2>
				<p>Om in te loggen in uw persoonlijke omgeving in het ChannelCMS gaat u naar <b>http://admin.branchechannel.com</b> en logt u in met de volgende gegevens:</p>
			</td>
		</tr>
		<tr>
			<td colspan="1">E-mailadres</td>
			<td colspan="3">{$klant.emailaddress}</td>
		</tr>
		<tr>
			<td colspan="1">Wachtwoord</td>
			<td colspan="3"><i>Het wachtwoord heeft u reeds ontvangen in de bevestigings e-mail</i></td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Heeft u de Branche Channel Player nog niet besteld?</h2>
				<p>Deze player kunt u online bestellen bij onze hardware-partner Multimediacenter via de webshop <b>http://branchechannel.multimediacenter.nl</b>. Deze kunt u gemakkelijk en veilig betalen met iDeal.</p>
			
				<p>Namens {$klant.groupname} en Branche Channel wensen wij u veel plezier en commercieel succes met {$klant.groupname}TV.</p>
				
				<p style="padding-top: 10px;">Met vriendelijke groet,</p>
				
				<p><img src="http://backend.branchechannel.com/template/images/factuur_signature.jpg" alt="" /></p>
				
				<p>Team Branche Channel</p>
			</td>
		</tr>
	</table>
</div>

</body>
</html>