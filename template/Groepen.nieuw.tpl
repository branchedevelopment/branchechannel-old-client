{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Nieuwe groep</h1>
		
		<div class="title">
			<h2>Gegevens</h2> 
		</div>
		<form method="post" enctype="multipart/form-data">
			<table>
				<tr>
					<th style="width: 20%;">Groepsnaam</th>
					<td><input type="text" name="bedrijfsnaam" /></th>
				</tr>
				<tr>
					<th>Contactpersoon</th>
					<td><input type="text" name="contactpersoon" value="" /> <input type="radio" name="aanhef" value="de heer" id="heer" /> <label for="heer">de heer</label> &nbsp; <input type="radio" name="aanhef" value="mevrouw" id="mevrouw" /> <label for="mevrouw">mevrouw</label></td>
				</tr>
				<tr>
					<th>E-mailadres</th>
					<td><input type="text" name="emailadres" /></td>
				</tr>
				<tr>
				    <th>Wachtwoord</th>
				    <td><input type="password" name="wachtwoord" /></td>
				</tr>
				<tr>
				    <th>Logo</th>
				    <td><input type="file" name="logo" /></td>
				</tr>
				<tr>
				    <th>Achtergrondkleur</th>
				    <td><input type="text" name="achtergrondkleur" id="achtergrondkleur" class="kleur" value="#000000" style="background-color: #000000; color: #ffffff;" readonly /></td>
				</tr>
				<tr>
				    <th>Tekstkleur</th>
				    <td><input type="text" name="tekstkleur" id="tekstkleur" class="kleur" value="#ffffff" style="background-color: #000000; color: #ffffff" readonly /></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<th></th>
					<td><input type="submit" name="add" value="Toevoegen" /> &nbsp; <input type="button" value="Annuleren" onclick="history.go(-1)" /></td>
					<th></th>
					<td></td>
				</tr>
			</table>
		</form>
{/if}
		
{include file="Main.footer.tpl"}