{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Dashboard</h1>
		
		<h2>Nieuwe aanmeldingen</h2>
		<table class="overview">
			<tr style="border: none;">
				<th>Klantnummer</th>
				<th>Bedrijfsnaam</th>
				<th>Groep</th>
				<th>Abonnement</th>
				<th>Channel</th>
				<th>Status</th>
				<th>Installatiehulp</th>
                <th>Actiecode</th>
                <th>Gewenste ingangsdatum</th>
	{if $Session->data.type == 1}
				<th style="text-align: right;">Opties</th>
	{/if}
			</tr>
{foreach $Page->aanmeldingen as $item}
			<tr>
				<td><a href="klanten/{$item.id}/bekijk/">{$item.id}</a></td>
				<td>{$item.company}</td>
				<td>{if $Session->data.type == 1}<a href="groepen/{$item.group}/bekijk/">{/if}{$item.groupname}{if $Session->data.type == 1}</a>{/if}</td>
				<td>{if $Session->data.type == 1}<a href="abonnementen/{$item.subscription}/bekijk/">{/if}{$item.subscriptionname}{if $Session->data.type == 1}</a>{/if}</td>
				<td><a href="http://client.branchechannel.com/?id={$item.hash}" target="_blank">url</a></td>
				<td>
					<form method="post">
						<select name="status" onchange="this.form.submit()">
							<option value="0">Onbehandeld</option>
							<option value="1"{if $item.player == 1} selected="selected"{/if}>Welkomstbrief</option>
							<option value="2">Player</option>
						</select>
						<input type="hidden" name="id" value="{$item.id}" />
					</form>
				</td>
				<td>{$item.installhelp}</td>
                <td>{$item.actioncode}</td>
                <td>{if $item.startdate}{date("d-m-Y", strtotime($item.startdate))}{/if}</td>
	{if $Session->data.type == 1}
				<td style="text-align: right;">
					<a href="dashboard/{$item.id}/brief/"><img src="template/images/icons/printer.png" alt="" title="Welkomsbrief uitprinten" /></a> 
					<a href="klanten/{$item.id}/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
					<a href="klanten/{$item.id}/status/"><img src="template/images/icons/{if $item.status}accept{else}delete{/if}.png" alt="" title="{if $item.status}Ina{else}A{/if}ctief maken" /></a> 
					<a href="klanten/{$item.id}/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
				</td>
	{/if}
			</tr>
{foreachelse}
			<tr>
				<td colspan="8"><em>Er zijn geen nieuwe aanmeldingen.</em></td>
			</tr>
{/foreach}
			<tr>
				<td colspan="5"></td>
				<td colspan="5"></td>
			</tr>
		</table>
		
		<!--
		<h2>Laatste tickets</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Ticketnummer</th>
				<th width="10%">Datum</th>
				<th width="10%">Groep</th>
				<th width="10%">Klantnummer</th>
				<th width="20%">Bedrijfsnaam</th>
				<th width="20%">Onderwerp</th>
				<th width="20%" style="text-align: right;">Status</th>
			</tr>
{foreach $Page->tickets as $item}
			<tr>
				<td><a href="">00001</a></td>
				<td>15-11-2012</td>
				<td><a href="">Redken</a></td>
				<td><a href="">00001</a></td>
				<td>Salon Bas</td>
				<td>Algemeen</td>
				<td style="text-align: right;"><select><option>Onbeantwoord</option><option>Beantwoord</option><option>Opgelost</option></select></td>
			</tr>
{foreachelse}
			<tr>
				<td colspan="7"><em>Er zijn momenteel geen tickets.</em></td>
			</tr>
{/foreach}
			<tr>
				<td colspan="4"></td>
				<td colspan="3"></td>
			</tr>
		</table>
		-->
{else}

		<h1>Inloggen</h1>
			<form method="post">
				<table>
					<tr>
						<th>E-mailadres</th>
						<td><input type="text" name="emailadres"/></td>
					</tr>
					<tr>
						<th>Wachtwoord</th>
						<td><input type="password" name="wachtwoord" /></td>
					</tr>
					<!--<tr>
						<th></th>
						<td><input type="checkbox" name="remember" value="1" /> <label>Ingelogd blijven</label></td>
					</tr>-->
					<tr>
						<th></th>
						<td><input type="submit" name="login" value="Inloggen" /></td>
					</tr>
					<!--<tr>
						<th></th>
						<td><a href="password/">Wachtwoord vergeten?</a></td>
					</tr>-->
				</table>
			</form>
		
{/if}
		
{include file="Main.footer.tpl"}