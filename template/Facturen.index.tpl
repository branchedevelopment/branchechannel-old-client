{include file="Main.header.tpl"}

{if $Session->data !== false}

		<div class="title">
			<h1>Factuuroverzicht</h1>
			<span class="options">Totaal: {$Page->facturen|count}</span>
			<div style="clear: both;"></div>
		</div>
		<!--<input type="search" name="" value="" placeholder="Zoeken..." />
		<div class="select"><a href="">Toon alles</a> | Groep <select><option>- Maak een keuze -</option><option>Redken</option></select></div>-->
		<div style="clear: both;"></div>
		
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Factuurnummer</th>
				<th width="10%">Factuurdatum</th>
				<th width="10%">Klantnummer</th>
				<th width="60%">Bedrijfsnaam</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
{foreach $Page->facturen as $item}
			<tr>
				<td><a href="facturen/{$item.fid}/factuur/">{$item.fdate|date_format:'%Y'}-{$item.klantnr}-{$item.fid}</a></td>
				<td>{$item.fdate|date_format:'%d-%m-%Y'}</td>
				<td><a href="klanten/{$item.klantnr}/bekijk/">{$item.klantnr}</a></td>
				<td>{$item.company}</td>
				<td style="text-align: right;">
					<a href="facturen/{$item.fid}/factuur/"><img src="template/images/icons/printer.png" alt="" title="Afdrukken" /></a> 
				</td>
			</tr>
{foreachelse}
			<tr>
				<td colspan="5"><em>Er zijn nog geen facturen.</em></td>
			</tr>
{/foreach}
			<tr>
				<td colspan="2"><!--Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina--></td>
				<td colspan="3"><!--1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a>--></td>
			</tr>
		</table>
		
{/if}
		
{include file="Main.footer.tpl"}