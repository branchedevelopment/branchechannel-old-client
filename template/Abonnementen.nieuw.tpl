{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Nieuw abonnement</h1>
		
		<div class="title">
			<h2>Gegevens</h2> 
		</div>
		<form method="post">
			<table>
				<tr>
					<th style="width: 20%;">Abonnementsnaam</th>
					<td><input type="text" name="name" /></th>
				</tr>
				<tr>
					<th style="width: 20%;">Groep</th>
					<td>
						<select name="group">
{foreach $Page->groepen as $item}
							<option value="{$item.id}">{$item.company|stripslashes}</option>
{/foreach}
						</select>
					</th>
				</tr>
				<tr>
					<th>Duur</th>
					<td>
						<select name="duration">
							<option>1 jaar</option>
							<option>2 jaar</option>
							<option>3 jaar</option>
							<option>4 jaar</option>
							<option>5 jaar</option>
							<option>Tot opzegging</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>Prijs per kwartaal (ex. btw) <span style="float: right; font-weight: normal;">&euro; &nbsp;</span></th>
					<td><input type="text" name="price" value="00,00" /></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<th></th>
					<td><input type="submit" name="add" value="Toevoegen" /> &nbsp; <input type="button" value="Annuleren" onclick="history.go(-1)" /></td>
					<th></th>
					<td></td>
				</tr>
			</table>
		</form>
{/if}
		
{include file="Main.footer.tpl"}