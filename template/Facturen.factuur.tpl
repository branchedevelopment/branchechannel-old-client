<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>BrancheChannel factuur</title>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10pt;
	color: #000;
	width: 100%;
	height: 100%;
}

#inhoud {
	clear: both;
}

table {
	clear: both;
	width: 100%;
	font-size: 10pt;
	color: #000;
}

table, th, td, tr {
	border: 0;
	border-collapse: collapse;
	vertical-align: top;
}

th {
	text-align: left;
}

td {
	padding: 5px 0px;
	color: #000;
}

h1 {
	margin: 0;
	padding: 0;
	font-size: 16pt;
}

p {
	margin-bottom: 20px;
	line-height: 20px;
}

</style>
</head>
<body>
<div id="inhoud">
	<table>
		<tr>
			<td colspan="4" style="text-align: right;"><img src="{$URL}template/images/factuur_logo.jpg" alt="" /></td>
		</tr>
		<tr>
			<th style="padding-top: 50px;" colspan="2">
				<h1>Factuur</h1>
			</th>
			<td></td>
		</tr>
		<tr>
			<td colspan="2">
				Periode {$Page->factuur.fdate|date_format:'%d %B %Y'} t/m {$Page->factuur.todate|date_format:'%d %B %Y'}
			</td>
			<td colspan="2">
				{$Page->factuur.company}<br />
				T.a.v. {$Page->factuur.title} {$Page->factuur.name}<br />
				{$Page->factuur.zipcode} {$Page->factuur.city}
			</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<th>Factuurnummer</th>
			<td colspan="3">{$Page->factuur.fdate|date_format:'%Y'}-{$Page->factuur.id}-{$Page->factuur.fid}</td>
		</tr>
		<tr>
			<th>Factuurdatum</th>
			<td colspan="3">{$Page->factuur.fdate|date_format:'%d-%m-%Y'}</td>
		</tr>
		<tr>
			<th>Klantnummer</th>
			<td colspan="3">{$Page->factuur.id}</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<th>Abonnement</th>
			<th>Tarief per maand</th>
			<th>Tarief per kwartaal</th>
			<th>Bedrag</th>
		</tr>
		<tr>
			<td>{$Page->factuur.subscription}</td>
			<td>&euro; {$Page->factuur.price}</td>
			<td>&euro; {$Page->factuur.price*3}</td>
			<td>&euro; {$Page->factuur.price*3}</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td colspan="2" style="padding: 0;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<th>Subtotaal</th>
			<td>&euro; {$Page->factuur.price*3}</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<th>BTW 21%</th>
			<td>&euro; {(($Page->factuur.price*3*21)/100)|number_format:'2':'.':''}</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td colspan="2" style="padding: 0;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<th>Totaal</th>
			<td>&euro; {(($Page->factuur.price*3*121)/100)|number_format:'2':'.':''}</td>
		</tr>
		<tr>
			<td colspan="4"><br /><br /></td>
		</tr>
		<tr>
			<th colspan="4">Het totaalbedrag zal binnen 14 dagen na factuurdatum automatisch worden afgeschreven van rekeningnummer {$Page->factuur.bank} onder vermelding van uw klantnummer en factuurnummer.</th>
		</tr>
	</table>
</div>
<table style="margin-top: 230px; font-size: 7pt; color: #666 !important; line-height: 20px;">
	<tr>
		<td colspan="5" style="font-size: 8pt;">Heeft u vragen over uw factuur? Mailt u deze dan naar invoice@branchechannel.com.</td>
	</tr>
	<tr>
		<td colspan="5" style="padding: 0;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</td>
	</tr>
	<tr>
		<td style="width: 20%;">
			Branche Channel BV<br />
			online signage solutions
		</td>
		<td style="width: 20%;">
			Weth. E . van Dronkelaarplein 3<br />
			7601 VZ Almelo
		</td>
		<td style="width: 20%;">
			info@branchechannel.com<br />
			www.branchechannel.com
		</td>
		<td style="width: 20%;">
			<b>KvK</b> 55991238<br />
			<b>BTW</b> NL851934924B01
		</td>
		<td style="width: 20%;">
			<b>RABO</b> 1472.52.296<br />
			<b>IBAN</b> &nbsp; NL79RABO0147252296
		</td>
	</tr>
	<tr>
		<td colspan="5" style="text-align: center; padding: 0;">
			Op alle verbintenissen met ons zijn, met uitsluiting van alle andere algemene voorwaarden van de wederpartij, onze algemene voorwaarden van toepassing.
		</td>
	</tr>
</table>
</body>
</html>