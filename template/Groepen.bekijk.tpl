{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Groep: {$Page->groep.company}</h1>
		{if $Page->groep.logo}
		<img src="http://content.branchechannel.com/groups/{$Page->groep.id}.png" alt="" />
		{/if}
		
		<div class="title">
			<h2>Gegevens</h2> 
			<div class="options">
				<a href="groepen/{$Page->groep.id}/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
				<a href="groepen/{$Page->groep.id}/status/"><img src="template/images/icons/{if $Page->groep.status}accept{else}delete{/if}.png" alt="" title="{if $Page->groep.status}Ina{else}A{/if}ctief maken" /></a> 
				<a href="groepen/{$Page->groep.id}/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
			</div>
		</div>	
		
		<table>
			<tr>
				<th style="width: 20%;">Groepsnaam</th>
				<td>{$Page->groep.company}</th>
			</tr>
			<tr>
				<th>Contactpersoon</th>
				<td>{$Page->groep.title} {$Page->groep.name}</td>
			</tr>
			<tr>
				<th>E-mailadres</th>
				<td><a href="mailto:{$Page->groep.emailaddress}">{$Page->groep.emailaddress}</a></td>
			</tr>
		</table>
		
		<h2>Abonnementen</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="30%">Abonnement</th>
				<th width="10%">Duur</th>
				<th width="20%">Aangesloten klanten</th>
				<th width="20%">Prijs per kwartaal (ex)</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
{foreach $Page->abonnementen as $item}
			<tr>
				<td><a href="">1 jarig</a></td>
				<td>1 jaar</td>
				<td>123</td>
				<td>&euro; 39,95</td>
				<td style="text-align: right;"><a href="abonnementen/{$item.id}/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> <a href="abonnementen/{$item.id}/status/"><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> <a href="abonnementen/{$item.id}/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a></td>
			</tr>
{foreachelse}
			<tr>
				<td colspan="6"><em>Er zijn geen abonnementen bij deze groep</em></td>
			</tr>
{/foreach}
			<tr>
				<td colspan="3"></td>
				<td colspan="3"></td>
			</tr>
		</table>
	
		<h2>Klanten</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Klantnummer</th>
				<th width="50%">Bedrijfsnaam</th>
				<th width="20%">Abonnement</th>
				<th width="10%">Channel</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
{foreach $Page->klanten as $item}
			<tr>
				<td><a href="klanten/bekijk/">00001</a></td>
				<td>Salon Bas</td>
				<td><a href="">3 jarig</a></td>
				<td><a href="">00001</a></td>
				<td style="text-align: right;"><a href="klanten/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> <a href=""><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> <a href=""><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a></td>
			</tr>
{foreachelse}
			<tr>
				<td colspan="6"><em>Er zijn geen klanten bij deze groep</em></td>
			</tr>
{/foreach}
			<tr>
				<td colspan="3"></td>
				<td colspan="3"></td>
			</tr>
		</table>
		
{/if}
		
{include file="Main.footer.tpl"}