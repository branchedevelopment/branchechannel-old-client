{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Klant: {$Page->klant.company}</h1>
		
		<div class="title">
			<h2>Gegevens bewerken</h2> 
		</div>
		<form method="post">
			<table>
				<tr>
					<th style="width: 20%;">Bedrijfsnaam</th>
					<td style="width: 50%;"><input type="text" name="bedrijfsnaam" value="{$Page->klant.company}" /></th>
					<th style="width: 15%;">Klantnummer</th>
					<td style="width: 15%;">{$Page->klant.id}</td>
				</tr>
				<tr>
					<th>Contactpersoon</th>
					<td><input type="text" name="contactpersoon" value="{$Page->klant.name}" /> <input type="radio" name="aanhef" value="de heer" id="heer"{if $Page->klant.title == 'de heer'} checked="checked"{/if} /> <label for="heer">de heer</label> &nbsp; <input type="radio" name="aanhef" value="mevrouw" id="mevrouw"{if $Page->klant.title == 'mevrouw'} checked="checked"{/if} /> <label for="mevrouw">mevrouw</label></td>
					<th>Groep</th>
					<td>
						<select name="group">
{foreach $Page->groepen as $item}
							<option value="{$item.id}"{if $Page->klant.group == $item.id} selected="selected"{/if}>{$item.company}</option>
{/foreach}
						</select>
					</td>
				</tr>
				<tr>
					<th>Straatnaam en huisnummer</th>
					<td><input type="text" name="straatnaam" value="{$Page->klant.street}" /> <input style="width: 60px;" type="text" name="huisnummer" value="{$Page->klant.number}" /></td>
					<th>Abonnement</th>
					<td>
						<select name="subscription">
{foreach $Page->abonnementen as $item}
							<option value="{$item.id}"{if $Page->klant.subscription == $item.id} selected="selected"{/if}>{$item.name}</option>
{/foreach}
						</select>
					</td>
				</tr>
				<tr>
					<th>Postcode en woonplaats</th>
					<td><input style="width: 60px;" type="text" name="postcode" value="{$Page->klant.zipcode}" /> <input type="text" name="woonplaats" value="{$Page->klant.city}" /></td>
					<th>Channel</th>
					<td><a href="http://client.branchechannel.com/?id={$Page->klant.hash}">url</a></td>
				</tr>
				<tr>
					<th>Bankrekeningnummer</th>
					<td><input type="text" name="bank" value="{$Page->klant.bank}" /></td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<th>KvK nummer</th>
					<td><input type="text" name="kvk" value="{$Page->klant.kvk}" /></td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<th>BTW nummer</th>
					<td><input type="text" name="btw" value="{$Page->klant.btw}" /></td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<th>E-mailadres</th>
					<td><input type="text" name="emailadres" value="{$Page->klant.emailaddress}" /></td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<th></th>
					<td><input type="submit" name="edit" value="Wijzigingen opslaan" /> &nbsp; <input type="button" value="Annuleren" onclick="history.go(-1)" /></td>
					<th></th>
					<td></td>
				</tr>
			</table>
		</form>
{/if}
		
{include file="Main.footer.tpl"}