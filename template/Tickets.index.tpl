{include file="Main.header.tpl"}

{if $Session->data !== false}

		<div class="title">
			<h1>Ticket overzicht</h1>
			<span class="options">Totaal: 72</span>
			<div style="clear: both;"></div>
		</div>
		<input type="search" name="" value="" placeholder="Zoeken..." />
		<div class="select"><a href="">Toon alles</a> | Groep <select><option>- Maak een keuze -</option><option>Redken</option></select></div>
		<div style="clear: both;"></div>
		
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Ticketnummer</th>
				<th width="10%">Datum</th>
				<th width="10%">Groep</th>
				<th width="10%">Klantnummer</th>
				<th width="20%">Bedrijfsnaam</th>
				<th width="20%">Onderwerp</th>
				<th width="20%" style="text-align: right;">Status</th>
			</tr>
			<tr>
				<td><a href="">00001</a></td>
				<td>15-11-2012</td>
				<td><a href="">Redken</a></td>
				<td><a href="">00001</a></td>
				<td>Salon Bas</td>
				<td>Algemeen</td>
				<td style="text-align: right;"><select><option>Onbeantwoord</option><option>Beantwoord</option><option>Opgelost</option></select></td>
			</tr>
			<tr>
				<td colspan="4"><!--Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina--></td>
				<td colspan="3"><!--1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a>--></td>
			</tr>
		</table>
		
{/if}
		
{include file="Main.footer.tpl"}