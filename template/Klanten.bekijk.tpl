{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Klant: {$Page->klant.company}</h1>
		
		<div class="title">
			<h2>Gegevens</h2> 
			{if $Session->data.type == 1}
			<div class="options">
				<a href="klanten/{$Page->klant.id}/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
				<a href="klanten/{$Page->klant.id}/status/"><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> 
				<a href="klanten/{$Page->klant.id}/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
			</div>
			{/if}
		</div>
		<table>
			<tr>
				<th style="width: 20%;">Bedrijfsnaam</th>
				<td style="width: 50%;">{$Page->klant.company}</th>
				<th style="width: 15%;">Klantnummer</th>
				<td style="width: 15%;">{$Page->klant.id}</td>
			</tr>
			<tr>
				<th>Contactpersoon</th>
				<td>{$Page->klant.title} {$Page->klant.name}</td>
				<th>Groep</th>
				<td>{if $Session->data.type == 1}<a href="groepen/{$Page->klant.group}/">{/if}{$Page->klant.groupname}{if $Session->data.type == 1}</a>{/if}</td>
			</tr>
			<tr>
				<th>Straatnaam en huisnummer</th>
				<td>{$Page->klant.street} {$Page->klant.number}</td>
				<th>Channel</th>
				<td><a href="http://client.branchechannel.com/?id={$Page->klant.hash}" target="_blank">url</a></td>
            </tr>
			<tr>
				<th>Postcode en woonplaats</th>
				<td>{$Page->klant.zipcode} {$Page->klant.city}</td>
				<th>Abonnement</th>
				<td>{if $Session->data.type == 1}<a href="abonnement/{$Page->klant.subscription}/">{/if}{$Page->klant.subscriptionname}{if $Session->data.type == 1}</a>{/if}</td>
			</tr>
			<tr>
				<th>Telefoonnummer</th>
				<td>{$Page->klant.phonenumber}</td>
				<th>Actiecode</th>
				<td>{$Page->klant.actioncode}</td>
			</tr>
			<tr>
				<th>Mobiel nummer</th>
				<td>{$Page->klant.mobilenumber}</td>
				<th>Gewenste Ingangsdatum</th>
				<td>{if $Page->klant.startdate}{date("d-m-Y", strtotime($Page->klant.startdate))}{/if}</td>
			</tr>
			<tr>
				<th>Bankrekeningnummer</th>
				<td>{$Page->klant.bank}</td>
				<th></th>
				<td></td>
			<tr>
				<th>KvK nummer</th>
				<td>{$Page->klant.kvk}</td>
				<th></th>
				<td></td>
			</tr>
			<tr>
				<th>BTW nummer</th>
				<td>{$Page->klant.btw}</td>
				<th></th>
				<td></td>
			</tr>
			<tr>
				<th>E-mailadres</th>
				<td><a href="mailto:{$Page->klant.emailaddress}">{$Page->klant.emailaddress}</a></td>
				<th></th>
				<td></td>
			</tr>
		</table>
		
{if $Session->data.type == 1}		
	
		<h2>Facturen</h2>
		
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Factuurnummer</th>
				<th width="80%">Factuurdatum</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
{foreach $Page->facturen as $item}
			<tr>
				<td><a href="">2012-00001-01</a></td>
				<td>15-11-2012</td>
				<td style="text-align: right;"><a href=""><img src="template/images/icons/printer.png" alt="" title="Afdrukken" /></a> <a href=""><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a></td>
			</tr>
{foreachelse}
			<tr>
				<td colspan="3"><em>Deze klant heeft nog geen facturen</em></td>
			</tr>
{/foreach}
			<tr>
				<td colspan="1"></td>
				<td colspan="2"></td>
			</tr>
		</table>

{/if}

		<h2>Tickets</h2>
		
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Ticketnummer</th>
				<th width="10%">Datum</th>
				<th width="10%">Groep</th>
				<th width="50%">Onderwerp</th>
				<th width="20%" style="text-align: right;">Status</th>
			</tr>
{foreach $Page->tickets as $item}
			<tr>
				<td><a href="">00001</a></td>
				<td>15-11-2012</td>
				<td><a href="">Redken</a></td>
				<td>Algemeen</td>
				<td style="text-align: right;"><select><option>Onbeantwoord</option><option>Beantwoord</option><option>Opgelost</option></select></td>
			</tr>
{foreachelse}
			<tr>
				<td colspan="5"><em>Deze klant heeft geen tickets</em></td>
			</tr>
{/foreach}
			<tr>
				<td colspan="2"></td>
				<td colspan="3"></td>
			</tr>
		</table>
{/if}
		
{include file="Main.footer.tpl"}