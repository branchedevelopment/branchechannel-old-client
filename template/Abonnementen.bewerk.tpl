{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Abonnement: 1 jarig</h1>
		
		<div class="title">
			<h2>Gegevens</h2> 
		</div>
		<form method="post">
			<table>
				<tr>
					<th style="width: 20%;">Abonnementsnaam</th>
					<td><input type="text" name="name" value="{$Page->abonnement.name|stripslashes}" /></th>
				</tr>
				<tr>
					<th style="width: 20%;">Groep</th>
					<td>
						<select name="group">
{foreach $Page->groepen as $item}
							<option value="{$item.id}"{if $Page->abonnement.group == $item.id} selected="selected"{/if}>{$item.company|stripslashes}</option>
{/foreach}
						</select>
					</th>
				</tr>
				<tr>
					<th>Duur</th>
					<td>
						<select name="duration">
							<option value="1"{if $Page->abonnement.duration == 1} selected="selected"{/if}>1 jaar</option>
							<option value="2"{if $Page->abonnement.duration == 2} selected="selected"{/if}>2 jaar</option>
							<option value="3"{if $Page->abonnement.duration == 3} selected="selected"{/if}>3 jaar</option>
							<option value="4"{if $Page->abonnement.duration == 4} selected="selected"{/if}>4 jaar</option>
							<option value="5"{if $Page->abonnement.duration == 5} selected="selected"{/if}>5 jaar</option>
							<option value="0"{if $Page->abonnement.duration == 0} selected="selected"{/if}>Tot opzegging</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>Prijs per kwartaal (ex. btw) <span style="float: right; font-weight: normal;">&euro; &nbsp;</span></th>
					<td><input type="text" name="price" value="{$Page->abonnement.price}" /></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<th></th>
					<td><input type="submit" name="edit" value="Wijzigingen opslaan" /> &nbsp; <input type="button" value="Annuleren" onclick="history.go(-1)" /></td>
					<th></th>
					<td></td>
				</tr>
			</table>
		</form>
{/if}
		
{include file="Main.footer.tpl"}