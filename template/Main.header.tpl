<!DOCTYPE HTML>
<html lang="nl">
<head>
<meta charset="utf-8" /> 
<meta name="keywords" content="" /> 
<meta name="description" content="" /> 
<meta name="robots" content="index, follow" /> 
<meta name="googlebot" content="index, follow" /> 
<base href="{$URL}" />
<title>Backend - Branche Channel</title>
<link href="template/css/style.css" rel="stylesheet" type="text/css" media="screen" />
<link href="template/css/colorpicker.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="template/js/colorpicker.js"></script>
<script type="text/javascript" src="template/js/functions.js"></script>
</head>
<body>

<div id="content" class="container">
	<div class="left"></div>
	<div class="content">
		<div id="header">
			<a class="logo" href="index.html"><img src="template/images/header_logo.png" alt="BrancheChannel" /></a>
	{if $Session->data !== false}
			<ul>
				<li><a href="index.html"{if $Main->op == 'home'} class="active"{/if}>Dashboard</a></li>
				<li><a href="klanten/"{if $Main->op == 'klanten'} class="active"{/if}>Klanten</a></li>
		{if $Session->data.type == 1}
				<li><a href="facturen/"{if $Main->op == 'facturen'} class="active"{/if}>Facturen</a></li>
				<!--<li><a href="tickets/"{if $Main->op == 'tickets'} class="active"{/if}>Tickets</a></li>-->
				<li><a href="groepen/"{if $Main->op == 'groepen'} class="active"{/if}>Groepen</a></li>
				<li><a href="abonnementen/"{if $Main->op == 'abonnementen'} class="active"{/if}>Abonnementen</a></li>
		{/if}
				<li><a href="uitloggen/">Uitloggen</a></li>
			</ul>
	{/if}
			<div style="clear:both"></div>
		</div>
		<div class="shadow"></div>
	