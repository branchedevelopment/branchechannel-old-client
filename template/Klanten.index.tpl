{include file="Main.header.tpl"}

{if $Session->data !== false}

		<div class="title">
			<h1>Klanten overzicht</h1>
			<span class="options">Totaal: {$Page->klanten|count}</span>
			<div style="clear: both;"></div>
		</div>
		<!--<input type="search" name="" value="" placeholder="Zoeken..." />
		<div class="select"><a href="">Toon alles</a> | Groep <select><option>- Maak een keuze -</option><option>REDKEN</option></select></div>-->
		<div style="clear: both;"></div>
		
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Klantnummer</th>
				<th width="20%">Bedrijfsnaam</th>
				<th width="10%">Bankrek. nr.</th>
				<th width="10%">Groep</th>
				<th width="15%">Abonnement</th>
				<th width="5%">Channel</th>
	{if $Session->data.type == 1}
				<th width="15%" style="text-align: right;">Opties</th>
	{/if}
			</tr>
{foreach $Page->klanten as $item}
			<tr>
				<td><a href="klanten/{$item.id}/bekijk/">{$item.id}</a></td>
				<td>{$item.company}</td>
				<td>{$item.bank}</td>
				<td>{if $Session->data.type == 1}<a href="groepen/{$item.group}/bekijk/">{/if}{$item.groupname}{if $Session->data.type == 1}</a>{/if}</td>
				<td>{if $Session->data.type == 1}<a href="abonnementen/{$item.subscription}/bekijk/">{/if}{$item.subscriptionname}{if $Session->data.type == 1}</a>{/if}</td>
				<td><a href="http://client.branchechannel.com/?id={$item.hash}" target="_blank">url</a></td>
	{if $Session->data.type == 1}
				<td style="text-align: right;">
					<a href="dashboard/{$item.id}/brief/"><img src="template/images/icons/printer.png" alt="" title="Welkomsbrief uitprinten" /></a> 
					<a href="klanten/{$item.id}/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
					<a href="klanten/{$item.id}/status/"><img src="template/images/icons/{if $item.status}accept{else}delete{/if}.png" alt="" title="{if $item.status}Ina{else}A{/if}ctief maken" /></a> 
					<a href="klanten/{$item.id}/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
				</td>
	{/if}
			</tr>
{/foreach}
			<tr>
				<td colspan="4"><!--Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina--></td>
				<td colspan="3"><!--1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a>--></td>
			</tr>
		</table>
		
{/if}
		
{include file="Main.footer.tpl"}