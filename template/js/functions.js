$(function() {

    $('#achtergrondkleur').ColorPicker({
    	color: '#000000',
    	onChange: function (hsb, hex, rgb) {
    		$('#achtergrondkleur').val('#' + hex);
    		$('.kleur').css('backgroundColor', '#' + hex);
    	}
    });
    
    $('#tekstkleur').ColorPicker({
    	color: '#ffffff',
    	onChange: function (hsb, hex, rgb) {
    		$('#tekstkleur').val('#' + hex);
    		$('.kleur').css('color', '#' + hex);
    	}
    });

});