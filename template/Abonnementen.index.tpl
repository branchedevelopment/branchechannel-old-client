{include file="Main.header.tpl"}

{if $Session->data !== false}

		<div class="title">
			<h1>Abonnementen overzicht</h1>
			<span class="options">Totaal: {$Page->abonnementen|count}</span>
			<div style="clear: both;"></div>
		</div>
		<!--<input type="search" name="" value="" placeholder="Zoeken..." />
		<div class="select"><a href="">Toon alles</a> | Groep <select><option>- Maak een keuze -</option><option>REDKEN</option></select></div>-->
		<div style="clear: both;"></div>
		
		<a href="abonnementen/nieuw/"><img src="template/images/icons/add.png" alt="" /> Abonnement toevoegen</a>

		<table style="margin-top: 20px;" class="overview">
			<tr style="border: none;">
				<th width="15%">Abonnement</th>
				<th width="15%">Groep</th>
				<th width="10%">Duur</th>
				<th width="20%">Aangesloten klanten</th>
				<th width="20%">Prijs per kwartaal (ex)</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
{foreach $Page->abonnementen as $item}
			<tr>
				<td><a href="abonnementen/{$item.id}/bekijk/">{$item.name}</a></td>
				<td><a href="groepen/{$item.group}/bekijk/">{$item.groupname}</td></td>
				<td>{$item.duration} jaar</td>
				<td>{$item.customers}</td>
				<td>&euro; {$item.price}</td>
				<td style="text-align: right;">
					<a href="abonnementen/{$item.id}/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
					<a href="abonnementen/{$item.id}/status/"><img src="template/images/icons/{if $item.status}accept{else}delete{/if}.png" alt="" title="{if $item.status}Ina{else}A{/if}ctief maken" /></a> 
					<a href="abonnementen/{$item.id}/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
				</td>
			</tr>
{/foreach}
			<tr>
				<td colspan="3"><!--Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina--></td>
				<td colspan="3"><!--1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a>--></td>
			</tr>
		</table>
		
{/if}
		
{include file="Main.footer.tpl"}