{include file="Main.header.tpl"}

{if $Session->data !== false}

		<h1>Abonnement: 1 jarig</h1>
		
		<div class="title">
			<h2>Gegevens</h2> 
			<div class="options"><a href="abonnementen/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> <a href=""><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> <a href=""><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a></div>
		</div>
		<table>
			<tr>
				<th style="width: 20%;">Abonnementsnaam</th>
				<td>1 jarig</th>
			</tr>
			<tr>
				<th>Groep</th>
				<td><a href="">Redken</a></td>
			</tr>
			<tr>
				<th>Duur</th>
				<td>1 jaar</td>
			</tr>
			<tr>
				<th>Prijs per kwartaal (ex. btw)</th>
				<td>&euro; 39,95</td>
			</tr>
		</table>
		
		<h2>Klanten</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Klantnummer</th>
				<th width="70%">Bedrijfsnaam</th>
				<th width="10%">Channel</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
			<tr>
				<td><a href="klanten/bekijk/">00001</a></td>
				<td>Salon Bas</td>
				<td><a href="">00001</a></td>
				<td style="text-align: right;"><a href="klanten/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> <a href=""><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> <a href=""><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a></td>
			</tr>
			<tr>
				<td colspan="2">Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina</td>
				<td colspan="2">1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a></td>
			</tr>
		</table>
		
{/if}
		
{include file="Main.footer.tpl"}