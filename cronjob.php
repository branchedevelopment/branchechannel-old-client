<?php

// define INDEX
define('INDEX', true);
define('BASEURL', dirname(__FILE__));

// Load config
require_once(BASEURL.'/config.php');

// Init
date_default_timezone_set(TIMEZONE);
setlocale(LC_TIME, LOCALE);
set_time_limit(0);

if(DEBUG) {
	ini_set('display_errors', 'On');
	error_reporting(E_ALL);
}

// Common files
require_once(BASEURL.'/classes/Database.class.php');
require_once(BASEURL.'/classes/Template.class.php');
require_once(BASEURL.'/classes/Mail.class.php');
require_once(BASEURL.'/classes/Main.class.php');



// Parse and display template
$Smarty->assign(array(
				'URL' => URL,
				'Main' => $Main
));

$content = $Smarty->fetch('Facturen.factuur.tpl');

/*$Mail->Subject = "Een project waar u donateur van bent is afgelopen.";
$Mail->MsgHTML($content);
$Mail->AddAddress('bob@realityweb.nl', 'Bob Olde Hampsink');
$Mail->Send();
$Mail->clearAddresses();*/

//$Smarty->display(ucfirst($Main->op).'.'.$Main->sub.'.tpl');

exit($content);

// Shut down
$Db->close();

?>