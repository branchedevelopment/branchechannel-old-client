<?php

// define INDEX
define('INDEX', true);
define('BASEURL', dirname(__FILE__));

// Load config
require_once(BASEURL.'/config.php');

// Init
date_default_timezone_set(TIMEZONE);
setlocale(LC_TIME, LOCALE);
set_time_limit(0);

if(DEBUG) {
	ini_set('display_errors', 'On');
	error_reporting(E_ALL);
}

// Common files
require_once(BASEURL.'/classes/Database.class.php');
require_once(BASEURL.'/classes/Template.class.php');
require_once(BASEURL.'/classes/Mail.class.php');
require_once(BASEURL.'/classes/Session.class.php');
require_once(BASEURL.'/classes/Main.class.php');

// Module
$Page = array();
if(file_exists(BASEURL.'/classes/'.ucfirst($Main->op).'.class.php')) {
	require_once(BASEURL.'/classes/'.ucfirst($Main->op).'.class.php');
}

// Parse and display template
$Smarty->assign(array(
				'Session' => $Session,
				'URL' => URL,
				'Main' => $Main,
				'Page' => $Page,
				'queries' => $Db->query_count
));

$Smarty->display(ucfirst($Main->op).'.'.$Main->sub.'.tpl');

// Shut down
$Db->close();

?>