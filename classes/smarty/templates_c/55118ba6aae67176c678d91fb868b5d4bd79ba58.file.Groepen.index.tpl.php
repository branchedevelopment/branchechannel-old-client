<?php /* Smarty version Smarty-3.1.8, created on 2013-09-13 10:52:25
         compiled from "/var/www/vhosts/backend.branchechannel.com/html/template/Groepen.index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13095086765232d2498e85a8-32871404%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '55118ba6aae67176c678d91fb868b5d4bd79ba58' => 
    array (
      0 => '/var/www/vhosts/backend.branchechannel.com/html/template/Groepen.index.tpl',
      1 => 1355401884,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13095086765232d2498e85a8-32871404',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'Session' => 0,
    'Page' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5232d24997bb30_05173561',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5232d24997bb30_05173561')) {function content_5232d24997bb30_05173561($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("Main.header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['Session']->value->data!==false){?>

		<div class="title">
			<h1>Groepen overzicht</h1>
			<span class="options">Totaal: <?php echo count($_smarty_tpl->tpl_vars['Page']->value->groepen);?>
</span>
			<div style="clear: both;"></div>
		</div>
		
		<a href="groepen/nieuw/"><img src="template/images/icons/add.png" alt="" /> Groep toevoegen</a>
		
		<table style="margin-top: 20px;" class="overview">
			<tr style="border: none;">
				<th width="50%">Groepsnaam</th>
				<th width="20%">Aangesloten abonnementen</th>
				<th width="20%">Aangesloten klanten</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->groepen; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="groepen/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/bekijk/"><?php echo $_smarty_tpl->tpl_vars['item']->value['company'];?>
</td></td>
				<td><?php echo $_smarty_tpl->tpl_vars['item']->value['subscriptions'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['item']->value['customers'];?>
</td>
				<td style="text-align: right;">
					<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
					<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/status/"><img src="template/images/icons/<?php if ($_smarty_tpl->tpl_vars['item']->value['status']){?>accept<?php }else{ ?>delete<?php }?>.png" alt="" title="<?php if ($_smarty_tpl->tpl_vars['item']->value['status']){?>Ina<?php }else{ ?>A<?php }?>ctief maken" /></a> 
					<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
				</td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="2"><!--Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina--></td>
				<td colspan="2"><!--1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a>--></td>
			</tr>
		</table>
		
<?php }?>
		
<?php echo $_smarty_tpl->getSubTemplate ("Main.footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>