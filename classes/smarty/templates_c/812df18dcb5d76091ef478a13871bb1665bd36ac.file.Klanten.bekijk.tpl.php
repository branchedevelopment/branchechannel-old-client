<?php /* Smarty version Smarty-3.1.8, created on 2014-04-10 10:49:32
         compiled from "/var/www/vhosts/backend.branchechannel.com/html/template/Klanten.bekijk.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1631614586522c85db6ab164-94128489%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '812df18dcb5d76091ef478a13871bb1665bd36ac' => 
    array (
      0 => '/var/www/vhosts/backend.branchechannel.com/html/template/Klanten.bekijk.tpl',
      1 => 1397118597,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1631614586522c85db6ab164-94128489',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_522c85db884993_61925115',
  'variables' => 
  array (
    'Session' => 0,
    'Page' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_522c85db884993_61925115')) {function content_522c85db884993_61925115($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("Main.header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['Session']->value->data!==false){?>

		<h1>Klant: <?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['company'];?>
</h1>
		
		<div class="title">
			<h2>Gegevens</h2> 
			<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?>
			<div class="options">
				<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['id'];?>
/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
				<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['id'];?>
/status/"><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> 
				<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['id'];?>
/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
			</div>
			<?php }?>
		</div>
		<table>
			<tr>
				<th style="width: 20%;">Bedrijfsnaam</th>
				<td style="width: 50%;"><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['company'];?>
</th>
				<th style="width: 15%;">Klantnummer</th>
				<td style="width: 15%;"><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['id'];?>
</td>
			</tr>
			<tr>
				<th>Contactpersoon</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['title'];?>
 <?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['name'];?>
</td>
				<th>Groep</th>
				<td><?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?><a href="groepen/<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['group'];?>
/"><?php }?><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['groupname'];?>
<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?></a><?php }?></td>
			</tr>
			<tr>
				<th>Straatnaam en huisnummer</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['number'];?>
</td>
				<th>Channel</th>
				<td><a href="http://client.branchechannel.com/?id=<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['hash'];?>
" target="_blank">url</a></td>
            </tr>
			<tr>
				<th>Postcode en woonplaats</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['zipcode'];?>
 <?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['city'];?>
</td>
				<th>Abonnement</th>
				<td><?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?><a href="abonnement/<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['subscription'];?>
/"><?php }?><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['subscriptionname'];?>
<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?></a><?php }?></td>
			</tr>
			<tr>
				<th>Telefoonnummer</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['phonenumber'];?>
</td>
				<th>Actiecode</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['actioncode'];?>
</td>
			</tr>
			<tr>
				<th>Mobiel nummer</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['mobilenumber'];?>
</td>
				<th>Gewenste Ingangsdatum</th>
				<td><?php if ($_smarty_tpl->tpl_vars['Page']->value->klant['startdate']){?><?php echo date("d-m-Y",strtotime($_smarty_tpl->tpl_vars['Page']->value->klant['startdate']));?>
<?php }?></td>
			</tr>
			<tr>
				<th>Bankrekeningnummer</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['bank'];?>
</td>
				<th></th>
				<td></td>
			<tr>
				<th>KvK nummer</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['kvk'];?>
</td>
				<th></th>
				<td></td>
			</tr>
			<tr>
				<th>BTW nummer</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['btw'];?>
</td>
				<th></th>
				<td></td>
			</tr>
			<tr>
				<th>E-mailadres</th>
				<td><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['emailaddress'];?>
"><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['emailaddress'];?>
</a></td>
				<th></th>
				<td></td>
			</tr>
		</table>
		
<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?>		
	
		<h2>Facturen</h2>
		
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Factuurnummer</th>
				<th width="80%">Factuurdatum</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->facturen; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="">2012-00001-01</a></td>
				<td>15-11-2012</td>
				<td style="text-align: right;"><a href=""><img src="template/images/icons/printer.png" alt="" title="Afdrukken" /></a> <a href=""><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a></td>
			</tr>
<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
			<tr>
				<td colspan="3"><em>Deze klant heeft nog geen facturen</em></td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="1"></td>
				<td colspan="2"></td>
			</tr>
		</table>

<?php }?>

		<h2>Tickets</h2>
		
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Ticketnummer</th>
				<th width="10%">Datum</th>
				<th width="10%">Groep</th>
				<th width="50%">Onderwerp</th>
				<th width="20%" style="text-align: right;">Status</th>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->tickets; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="">00001</a></td>
				<td>15-11-2012</td>
				<td><a href="">Redken</a></td>
				<td>Algemeen</td>
				<td style="text-align: right;"><select><option>Onbeantwoord</option><option>Beantwoord</option><option>Opgelost</option></select></td>
			</tr>
<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
			<tr>
				<td colspan="5"><em>Deze klant heeft geen tickets</em></td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="2"></td>
				<td colspan="3"></td>
			</tr>
		</table>
<?php }?>
		
<?php echo $_smarty_tpl->getSubTemplate ("Main.footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>