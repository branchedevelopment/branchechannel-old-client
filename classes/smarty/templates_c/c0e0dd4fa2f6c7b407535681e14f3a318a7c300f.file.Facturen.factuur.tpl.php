<?php /* Smarty version Smarty-3.1.8, created on 2013-10-09 15:28:05
         compiled from "/var/www/vhosts/backend.branchechannel.com/html/template/Facturen.factuur.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1883695441525559e5629798-87199096%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c0e0dd4fa2f6c7b407535681e14f3a318a7c300f' => 
    array (
      0 => '/var/www/vhosts/backend.branchechannel.com/html/template/Facturen.factuur.tpl',
      1 => 1356700807,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1883695441525559e5629798-87199096',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'URL' => 0,
    'Page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_525559e58a2257_33469072',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525559e58a2257_33469072')) {function content_525559e58a2257_33469072($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhosts/backend.branchechannel.com/html/classes/smarty/plugins/modifier.date_format.php';
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>BrancheChannel factuur</title>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10pt;
	color: #000;
	width: 100%;
	height: 100%;
}

#inhoud {
	clear: both;
}

table {
	clear: both;
	width: 100%;
	font-size: 10pt;
	color: #000;
}

table, th, td, tr {
	border: 0;
	border-collapse: collapse;
	vertical-align: top;
}

th {
	text-align: left;
}

td {
	padding: 5px 0px;
	color: #000;
}

h1 {
	margin: 0;
	padding: 0;
	font-size: 16pt;
}

p {
	margin-bottom: 20px;
	line-height: 20px;
}

</style>
</head>
<body>
<div id="inhoud">
	<table>
		<tr>
			<td colspan="4" style="text-align: right;"><img src="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
template/images/factuur_logo.jpg" alt="" /></td>
		</tr>
		<tr>
			<th style="padding-top: 50px;" colspan="2">
				<h1>Factuur</h1>
			</th>
			<td></td>
		</tr>
		<tr>
			<td colspan="2">
				Periode <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['Page']->value->factuur['fdate'],'%d %B %Y');?>
 t/m <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['Page']->value->factuur['todate'],'%d %B %Y');?>

			</td>
			<td colspan="2">
				<?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['company'];?>
<br />
				T.a.v. <?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['title'];?>
 <?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['name'];?>
<br />
				<?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['zipcode'];?>
 <?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['city'];?>

			</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<th>Factuurnummer</th>
			<td colspan="3"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['Page']->value->factuur['fdate'],'%Y');?>
-<?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['id'];?>
-<?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['fid'];?>
</td>
		</tr>
		<tr>
			<th>Factuurdatum</th>
			<td colspan="3"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['Page']->value->factuur['fdate'],'%d-%m-%Y');?>
</td>
		</tr>
		<tr>
			<th>Klantnummer</th>
			<td colspan="3"><?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['id'];?>
</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<th>Abonnement</th>
			<th>Tarief per maand</th>
			<th>Tarief per kwartaal</th>
			<th>Bedrag</th>
		</tr>
		<tr>
			<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['subscription'];?>
</td>
			<td>&euro; <?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['price'];?>
</td>
			<td>&euro; <?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['price']*3;?>
</td>
			<td>&euro; <?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['price']*3;?>
</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td colspan="2" style="padding: 0;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<th>Subtotaal</th>
			<td>&euro; <?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['price']*3;?>
</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<th>BTW 21%</th>
			<td>&euro; <?php echo number_format((($_smarty_tpl->tpl_vars['Page']->value->factuur['price']*3*21)/100),'2','.','');?>
</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td colspan="2" style="padding: 0;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<th>Totaal</th>
			<td>&euro; <?php echo number_format((($_smarty_tpl->tpl_vars['Page']->value->factuur['price']*3*121)/100),'2','.','');?>
</td>
		</tr>
		<tr>
			<td colspan="4"><br /><br /></td>
		</tr>
		<tr>
			<th colspan="4">Het totaalbedrag zal binnen 14 dagen na factuurdatum automatisch worden afgeschreven van rekeningnummer <?php echo $_smarty_tpl->tpl_vars['Page']->value->factuur['bank'];?>
 onder vermelding van uw klantnummer en factuurnummer.</th>
		</tr>
	</table>
</div>
<table style="margin-top: 230px; font-size: 7pt; color: #666 !important; line-height: 20px;">
	<tr>
		<td colspan="5" style="font-size: 8pt;">Heeft u vragen over uw factuur? Mailt u deze dan naar invoice@branchechannel.com.</td>
	</tr>
	<tr>
		<td colspan="5" style="padding: 0;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</td>
	</tr>
	<tr>
		<td style="width: 20%;">
			Branche Channel BV<br />
			online signage solutions
		</td>
		<td style="width: 20%;">
			Weth. E . van Dronkelaarplein 3<br />
			7601 VZ Almelo
		</td>
		<td style="width: 20%;">
			info@branchechannel.com<br />
			www.branchechannel.com
		</td>
		<td style="width: 20%;">
			<b>KvK</b> 55991238<br />
			<b>BTW</b> NL851934924B01
		</td>
		<td style="width: 20%;">
			<b>RABO</b> 1472.52.296<br />
			<b>IBAN</b> &nbsp; NL79RABO0147252296
		</td>
	</tr>
	<tr>
		<td colspan="5" style="text-align: center; padding: 0;">
			Op alle verbintenissen met ons zijn, met uitsluiting van alle andere algemene voorwaarden van de wederpartij, onze algemene voorwaarden van toepassing.
		</td>
	</tr>
</table>
</body>
</html><?php }} ?>