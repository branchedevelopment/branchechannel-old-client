<?php /* Smarty version Smarty-3.1.8, created on 2013-02-21 09:21:00
         compiled from "/var/www/vhosts/branchechannel.com/backend/template/Dashboard.index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14836620750af887dbe08f5-51657181%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6d5df683dc76d43fee37925619ff9212b088bff' => 
    array (
      0 => '/var/www/vhosts/branchechannel.com/backend/template/Dashboard.index.tpl',
      1 => 1361434859,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14836620750af887dbe08f5-51657181',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_50af887dc92ab3_96386197',
  'variables' => 
  array (
    'Session' => 0,
    'Page' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_50af887dc92ab3_96386197')) {function content_50af887dc92ab3_96386197($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("Main.header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['Session']->value->data!==false){?>

		<h1>Dashboard</h1>
		
		<h2>Nieuwe aanmeldingen</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Klantnummer</th>
				<th width="15%">Bedrijfsnaam</th>
				<th width="10%">Groep</th>
				<th width="15%">Abonnement</th>
				<th width="10%">Channel</th>
				<th width="15%">Status</th>
				<th width="10%">Installatiehulp</th>
	<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?>
				<th width="15%" style="text-align: right;">Opties</th>
	<?php }?>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->aanmeldingen; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="klanten/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/bekijk/"><?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
</a></td>
				<td><?php echo $_smarty_tpl->tpl_vars['item']->value['company'];?>
</td>
				<td><?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?><a href="groepen/<?php echo $_smarty_tpl->tpl_vars['item']->value['group'];?>
/bekijk/"><?php }?><?php echo $_smarty_tpl->tpl_vars['item']->value['groupname'];?>
<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?></a><?php }?></td>
				<td><?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?><a href="abonnementen/<?php echo $_smarty_tpl->tpl_vars['item']->value['subscription'];?>
/bekijk/"><?php }?><?php echo $_smarty_tpl->tpl_vars['item']->value['subscriptionname'];?>
<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?></a><?php }?></td>
				<td><a href="http://client.branchechannel.com/?id=<?php echo $_smarty_tpl->tpl_vars['item']->value['hash'];?>
" target="_blank">url</a></td>
				<td>
					<form method="post">
						<select name="status" onchange="this.form.submit()">
							<option value="0">Onbehandeld</option>
							<option value="1"<?php if ($_smarty_tpl->tpl_vars['item']->value['player']==1){?> selected="selected"<?php }?>>Welkomsbrief verstuurd</option>
							<option value="2">Player verstuurd</option>
						</select>
						<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" />
					</form>
				</td>
				<td><?php echo $_smarty_tpl->tpl_vars['item']->value['installhelp'];?>
</td>
	<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?>
				<td style="text-align: right;">
					<a href="dashboard/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/brief/"><img src="template/images/icons/printer.png" alt="" title="Welkomsbrief uitprinten" /></a> 
					<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
					<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/status/"><img src="template/images/icons/<?php if ($_smarty_tpl->tpl_vars['item']->value['status']){?>accept<?php }else{ ?>delete<?php }?>.png" alt="" title="<?php if ($_smarty_tpl->tpl_vars['item']->value['status']){?>Ina<?php }else{ ?>A<?php }?>ctief maken" /></a> 
					<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
				</td>
	<?php }?>
			</tr>
<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
			<tr>
				<td colspan="8"><em>Er zijn geen nieuwe aanmeldingen.</em></td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="4"></td>
				<td colspan="4"></td>
			</tr>
		</table>
		
		<!--
		<h2>Laatste tickets</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Ticketnummer</th>
				<th width="10%">Datum</th>
				<th width="10%">Groep</th>
				<th width="10%">Klantnummer</th>
				<th width="20%">Bedrijfsnaam</th>
				<th width="20%">Onderwerp</th>
				<th width="20%" style="text-align: right;">Status</th>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->tickets; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="">00001</a></td>
				<td>15-11-2012</td>
				<td><a href="">Redken</a></td>
				<td><a href="">00001</a></td>
				<td>Salon Bas</td>
				<td>Algemeen</td>
				<td style="text-align: right;"><select><option>Onbeantwoord</option><option>Beantwoord</option><option>Opgelost</option></select></td>
			</tr>
<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
			<tr>
				<td colspan="7"><em>Er zijn momenteel geen tickets.</em></td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="4"></td>
				<td colspan="3"></td>
			</tr>
		</table>
		-->
<?php }else{ ?>

		<h1>Inloggen</h1>
			<form method="post">
				<table>
					<tr>
						<th>E-mailadres</th>
						<td><input type="text" name="emailadres"/></td>
					</tr>
					<tr>
						<th>Wachtwoord</th>
						<td><input type="password" name="wachtwoord" /></td>
					</tr>
					<!--<tr>
						<th></th>
						<td><input type="checkbox" name="remember" value="1" /> <label>Ingelogd blijven</label></td>
					</tr>-->
					<tr>
						<th></th>
						<td><input type="submit" name="login" value="Inloggen" /></td>
					</tr>
					<!--<tr>
						<th></th>
						<td><a href="password/">Wachtwoord vergeten?</a></td>
					</tr>-->
				</table>
			</form>
		
<?php }?>
		
<?php echo $_smarty_tpl->getSubTemplate ("Main.footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>