<?php /* Smarty version Smarty-3.1.8, created on 2013-09-07 16:30:37
         compiled from "/var/www/vhosts/backend.branchechannel.com/html/template/Klanten.index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1344910022522b388d0d7cf5-78693120%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '73bbc3faf730a1fcd8d7f6e94ffcde463a5d970f' => 
    array (
      0 => '/var/www/vhosts/backend.branchechannel.com/html/template/Klanten.index.tpl',
      1 => 1366703434,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1344910022522b388d0d7cf5-78693120',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'Session' => 0,
    'Page' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_522b388d1fb2a7_14822592',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_522b388d1fb2a7_14822592')) {function content_522b388d1fb2a7_14822592($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("Main.header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['Session']->value->data!==false){?>

		<div class="title">
			<h1>Klanten overzicht</h1>
			<span class="options">Totaal: <?php echo count($_smarty_tpl->tpl_vars['Page']->value->klanten);?>
</span>
			<div style="clear: both;"></div>
		</div>
		<!--<input type="search" name="" value="" placeholder="Zoeken..." />
		<div class="select"><a href="">Toon alles</a> | Groep <select><option>- Maak een keuze -</option><option>REDKEN</option></select></div>-->
		<div style="clear: both;"></div>
		
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Klantnummer</th>
				<th width="20%">Bedrijfsnaam</th>
				<th width="10%">Bankrek. nr.</th>
				<th width="10%">Groep</th>
				<th width="15%">Abonnement</th>
				<th width="5%">Channel</th>
	<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?>
				<th width="15%" style="text-align: right;">Opties</th>
	<?php }?>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->klanten; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="klanten/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/bekijk/"><?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
</a></td>
				<td><?php echo $_smarty_tpl->tpl_vars['item']->value['company'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['item']->value['bank'];?>
</td>
				<td><?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?><a href="groepen/<?php echo $_smarty_tpl->tpl_vars['item']->value['group'];?>
/bekijk/"><?php }?><?php echo $_smarty_tpl->tpl_vars['item']->value['groupname'];?>
<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?></a><?php }?></td>
				<td><?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?><a href="abonnementen/<?php echo $_smarty_tpl->tpl_vars['item']->value['subscription'];?>
/bekijk/"><?php }?><?php echo $_smarty_tpl->tpl_vars['item']->value['subscriptionname'];?>
<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?></a><?php }?></td>
				<td><a href="http://client.branchechannel.com/?id=<?php echo $_smarty_tpl->tpl_vars['item']->value['hash'];?>
" target="_blank">url</a></td>
	<?php if ($_smarty_tpl->tpl_vars['Session']->value->data['type']==1){?>
				<td style="text-align: right;">
					<a href="dashboard/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/brief/"><img src="template/images/icons/printer.png" alt="" title="Welkomsbrief uitprinten" /></a> 
					<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
					<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/status/"><img src="template/images/icons/<?php if ($_smarty_tpl->tpl_vars['item']->value['status']){?>accept<?php }else{ ?>delete<?php }?>.png" alt="" title="<?php if ($_smarty_tpl->tpl_vars['item']->value['status']){?>Ina<?php }else{ ?>A<?php }?>ctief maken" /></a> 
					<a href="klanten/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
				</td>
	<?php }?>
			</tr>
<?php } ?>
			<tr>
				<td colspan="4"><!--Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina--></td>
				<td colspan="3"><!--1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a>--></td>
			</tr>
		</table>
		
<?php }?>
		
<?php echo $_smarty_tpl->getSubTemplate ("Main.footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>