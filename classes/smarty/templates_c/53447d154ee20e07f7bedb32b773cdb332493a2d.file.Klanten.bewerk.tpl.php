<?php /* Smarty version Smarty-3.1.8, created on 2013-02-07 08:41:05
         compiled from "/var/www/vhosts/branchechannel.com/backend/template/Klanten.bewerk.tpl" */ ?>
<?php /*%%SmartyHeaderCode:192562369350acd785313800-82716221%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '53447d154ee20e07f7bedb32b773cdb332493a2d' => 
    array (
      0 => '/var/www/vhosts/branchechannel.com/backend/template/Klanten.bewerk.tpl',
      1 => 1359551720,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '192562369350acd785313800-82716221',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_50acd785364029_05445824',
  'variables' => 
  array (
    'Session' => 0,
    'Page' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_50acd785364029_05445824')) {function content_50acd785364029_05445824($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("Main.header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['Session']->value->data!==false){?>

		<h1>Klant: <?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['company'];?>
</h1>
		
		<div class="title">
			<h2>Gegevens bewerken</h2> 
		</div>
		<form method="post">
			<table>
				<tr>
					<th style="width: 20%;">Bedrijfsnaam</th>
					<td style="width: 50%;"><input type="text" name="bedrijfsnaam" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['company'];?>
" /></th>
					<th style="width: 15%;">Klantnummer</th>
					<td style="width: 15%;"><?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['id'];?>
</td>
				</tr>
				<tr>
					<th>Contactpersoon</th>
					<td><input type="text" name="contactpersoon" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['name'];?>
" /> <input type="radio" name="aanhef" value="de heer" id="heer"<?php if ($_smarty_tpl->tpl_vars['Page']->value->klant['title']=='de heer'){?> checked="checked"<?php }?> /> <label for="heer">de heer</label> &nbsp; <input type="radio" name="aanhef" value="mevrouw" id="mevrouw"<?php if ($_smarty_tpl->tpl_vars['Page']->value->klant['title']=='mevrouw'){?> checked="checked"<?php }?> /> <label for="mevrouw">mevrouw</label></td>
					<th>Groep</th>
					<td>
						<select name="group">
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->groepen; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['Page']->value->klant['group']==$_smarty_tpl->tpl_vars['item']->value['id']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['company'];?>
</option>
<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<th>Straatnaam en huisnummer</th>
					<td><input type="text" name="straatnaam" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['street'];?>
" /> <input style="width: 60px;" type="text" name="huisnummer" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['number'];?>
" /></td>
					<th>Abonnement</th>
					<td>
						<select name="subscription">
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->abonnementen; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['Page']->value->klant['subscription']==$_smarty_tpl->tpl_vars['item']->value['id']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</option>
<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<th>Postcode en woonplaats</th>
					<td><input style="width: 60px;" type="text" name="postcode" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['zipcode'];?>
" /> <input type="text" name="woonplaats" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['city'];?>
" /></td>
					<th>Channel</th>
					<td><a href="http://client.branchechannel.com/?id=<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['hash'];?>
">url</a></td>
				</tr>
				<tr>
					<th>Bankrekeningnummer</th>
					<td><input type="text" name="bank" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['bank'];?>
" /></td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<th>KvK nummer</th>
					<td><input type="text" name="kvk" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['kvk'];?>
" /></td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<th>BTW nummer</th>
					<td><input type="text" name="btw" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['btw'];?>
" /></td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<th>E-mailadres</th>
					<td><input type="text" name="emailadres" value="<?php echo $_smarty_tpl->tpl_vars['Page']->value->klant['emailaddress'];?>
" /></td>
					<th></th>
					<td></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<th></th>
					<td><input type="submit" name="edit" value="Wijzigingen opslaan" /> &nbsp; <input type="button" value="Annuleren" onclick="history.go(-1)" /></td>
					<th></th>
					<td></td>
				</tr>
			</table>
		</form>
<?php }?>
		
<?php echo $_smarty_tpl->getSubTemplate ("Main.footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>