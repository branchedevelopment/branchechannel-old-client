<?php /* Smarty version Smarty-3.1.8, created on 2014-02-28 07:39:18
         compiled from "/var/www/vhosts/backend.branchechannel.com/html/template/Groepen.bekijk.tpl" */ ?>
<?php /*%%SmartyHeaderCode:135153793752b016b7bcd836-43305033%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5f13decdbf956596bbad10f58add0d495a613f04' => 
    array (
      0 => '/var/www/vhosts/backend.branchechannel.com/html/template/Groepen.bekijk.tpl',
      1 => 1393428117,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '135153793752b016b7bcd836-43305033',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_52b016b7d79e67_48325550',
  'variables' => 
  array (
    'Session' => 0,
    'Page' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52b016b7d79e67_48325550')) {function content_52b016b7d79e67_48325550($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("Main.header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['Session']->value->data!==false){?>

		<h1>Groep: <?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['company'];?>
</h1>
		<?php if ($_smarty_tpl->tpl_vars['Page']->value->groep['logo']){?>
		<img src="http://content.branchechannel.com/groups/<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['id'];?>
.png" alt="" />
		<?php }?>
		
		<div class="title">
			<h2>Gegevens</h2> 
			<div class="options">
				<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['id'];?>
/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
				<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['id'];?>
/status/"><img src="template/images/icons/<?php if ($_smarty_tpl->tpl_vars['Page']->value->groep['status']){?>accept<?php }else{ ?>delete<?php }?>.png" alt="" title="<?php if ($_smarty_tpl->tpl_vars['Page']->value->groep['status']){?>Ina<?php }else{ ?>A<?php }?>ctief maken" /></a> 
				<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['id'];?>
/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
			</div>
		</div>	
		
		<table>
			<tr>
				<th style="width: 20%;">Groepsnaam</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['company'];?>
</th>
			</tr>
			<tr>
				<th>Contactpersoon</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['title'];?>
 <?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['name'];?>
</td>
			</tr>
			<tr>
				<th>E-mailadres</th>
				<td><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['emailaddress'];?>
"><?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['emailaddress'];?>
</a></td>
			</tr>
		</table>
		
		<h2>Abonnementen</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="30%">Abonnement</th>
				<th width="10%">Duur</th>
				<th width="20%">Aangesloten klanten</th>
				<th width="20%">Prijs per kwartaal (ex)</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->abonnementen; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="">1 jarig</a></td>
				<td>1 jaar</td>
				<td>123</td>
				<td>&euro; 39,95</td>
				<td style="text-align: right;"><a href="abonnementen/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> <a href="abonnementen/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/status/"><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> <a href="abonnementen/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a></td>
			</tr>
<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
			<tr>
				<td colspan="6"><em>Er zijn geen abonnementen bij deze groep</em></td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="3"></td>
				<td colspan="3"></td>
			</tr>
		</table>
	
		<h2>Klanten</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Klantnummer</th>
				<th width="50%">Bedrijfsnaam</th>
				<th width="20%">Abonnement</th>
				<th width="10%">Channel</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->klanten; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="klanten/bekijk/">00001</a></td>
				<td>Salon Bas</td>
				<td><a href="">3 jarig</a></td>
				<td><a href="">00001</a></td>
				<td style="text-align: right;"><a href="klanten/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> <a href=""><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> <a href=""><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a></td>
			</tr>
<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
			<tr>
				<td colspan="6"><em>Er zijn geen klanten bij deze groep</em></td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="3"></td>
				<td colspan="3"></td>
			</tr>
		</table>
		
<?php }?>
		
<?php echo $_smarty_tpl->getSubTemplate ("Main.footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>