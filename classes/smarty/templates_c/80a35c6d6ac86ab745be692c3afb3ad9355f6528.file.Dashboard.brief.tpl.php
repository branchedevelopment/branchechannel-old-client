<?php /* Smarty version Smarty-3.1.8, created on 2013-03-06 16:54:35
         compiled from "/var/www/vhosts/branchechannel.com/backend/template/Dashboard.brief.tpl" */ ?>
<?php /*%%SmartyHeaderCode:106299831250bf47318c2339-20746116%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '80a35c6d6ac86ab745be692c3afb3ad9355f6528' => 
    array (
      0 => '/var/www/vhosts/branchechannel.com/backend/template/Dashboard.brief.tpl',
      1 => 1362585220,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '106299831250bf47318c2339-20746116',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_50bf47318f8dc3_39798968',
  'variables' => 
  array (
    'klant' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_50bf47318f8dc3_39798968')) {function content_50bf47318f8dc3_39798968($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhosts/branchechannel.com/backend/classes/smarty/plugins/modifier.date_format.php';
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Branche Channel</title>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9pt;
	color: #000;
	width: 100%;
	height: 100%;
}

.inhoud {
	clear: both;
	margin: 140px 20px 20px 20px;
}

table {
	clear: both;
	width: 100%;
	font-size: 9pt;
	color: #000;
}

table, th, td, tr {
	border: 0;
	border-collapse: collapse;
	vertical-align: top;
}

th {
	text-align: left;
}

td {
	padding: 5px 0px;
	color: #000;
}

h1 {
	margin: 0 0 5px 0;
	padding: 0;
	font-size: 16pt;
	font-weight: normal;
}

h2 {
	margin: 20px 0 5px 0;
	padding: 0;
	font-size: 12pt;
	font-weight: normal;
}

p {
	margin: 0 0 10px 0;
	line-height: 12pt;
}

</style>
</head>
<body>
<div class="inhoud">
	<table>
		<tr>
			<td colspan="4">
				<div style="padding: 30px 0px 0px 30px">
					<?php echo $_smarty_tpl->tpl_vars['klant']->value['company'];?>
<br =/>
					T.a.v. <?php echo $_smarty_tpl->tpl_vars['klant']->value['title'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['name'];?>
<br />
					<?php echo $_smarty_tpl->tpl_vars['klant']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['number'];?>
<br />
					<?php echo $_smarty_tpl->tpl_vars['klant']->value['zipcode'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['city'];?>

				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px; text-align: right;">Almelo, <?php echo smarty_modifier_date_format(time(),'%e %B %Y');?>
</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px;">
				<h1>Hartelijk welkom bij <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
TV</h1>
				<p>Geachte <?php echo $_smarty_tpl->tpl_vars['klant']->value['title'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['name'];?>
</p>
				
				<p>Een aantal dagen geleden heeft u zich geregistreerd voor <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
TV en wij willen u ook namens <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
 hartelijk welkom heten bij Branche Channel. Branche Channel levert het ChannelCMS, het online beheersysteem, waarmee u uw eigen RedkenTV kunt onderhouden.</p>
				
				<p>Wilt u meer uitleg over ons ChannelCMS dan verwijzen wij u graag naar onze instructievideo's welke u online kunt bekijken op <b>http://<?php echo mb_strtolower($_smarty_tpl->tpl_vars['klant']->value['groupname'], 'UTF-8');?>
.branchechannel.com/tutorials</b>.</p>
				
				<h2>Uw inloggegevens</h2>
				<p>Om in te loggen in uw persoonlijke omgeving in het ChannelCMS gaat u naar <b>http://admin.branchechannel.com</b> en logt u in met de volgende gegevens:</p>
			</td>
		</tr>
		<tr>
			<td colspan="1">E-mailadres</td>
			<td colspan="3"><?php echo $_smarty_tpl->tpl_vars['klant']->value['emailaddress'];?>
</td>
		</tr>
		<tr>
			<td colspan="1">Wachtwoord</td>
			<td colspan="3"><i>Het wachtwoord heeft u reeds ontvangen in de bevestigings e-mail</i></td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Heeft u de Branche Channel Player nog niet besteld?</h2>
				<p>Deze player kunt u online bestellen bij onze hardware-partner Multimediacenter via de webshop <b>http://branchechannel.multimediacenter.nl</b>. Deze kunt u gemakkelijk en veilig betalen met iDeal.</p>
			
			
				<h2>Machtiging automatische incasso</h2>
				<p>Tevens treft u het machtigingsformulier aan voor een doorlopende automatische incasso voor de maandelijkse abonnementskosten welke elke kwartaal worden ge&iuml;nd. Wij verzoeken u vriendelijk dit formulier verder in te vullen en deze ondertekend aan ons te retourneren middels bijgaande retourenveloppe. U hoeft deze niet te frankeren.</p>
			
				<p>Namens <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
 en Branche Channel wensen wij u veel plezier en commercieel succes met <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
TV.</p>
				
				<p style="padding-top: 10px;">Met vriendelijke groet,</p>
				
				<p><img src="http://backend.branchechannel.com/template/images/factuur_signature.jpg" alt="" /></p>
				
				<p>Team Branche Channel</p>
				<p style="font-size: 8pt;">Bijlage: machtigingsformulier doorlopende automatische incasso &amp; retourenveloppe</p>
			</td>
		</tr>
	</table>
</div>

<div style="page-break-after: always"></div>

<div class="inhoud">
	<table>
		<tr>
			<td colspan="4">
				<div style="padding: 30px 0px 0px 30px">
					Branche Channel BV<br />
					T.a.v. de administratie<br />
					Weth. E van Dronkelaarplein 3<br />
					7601 VZ Almelo
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px; text-align: right;">Almelo, <?php echo smarty_modifier_date_format(time(),'%e %B %Y');?>
</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px;">
				<h1>Machtiging</h1>
				<p><b>Doorlopende automatische incasso</b></p>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Gegevens opdrachtgever</h2>
			</td>
		</tr>
		<tr>
			<td colspan="1">Bedrijf</td>
			<td colspan="3"><?php echo $_smarty_tpl->tpl_vars['klant']->value['company'];?>
</td>
		</tr>
		<tr>
			<td colspan="1">Naam</td>
			<td colspan="3"><?php echo $_smarty_tpl->tpl_vars['klant']->value['name'];?>
</td>
		</tr>
		<tr>
			<td colspan="1">Adres</td>
			<td colspan="3"><?php echo $_smarty_tpl->tpl_vars['klant']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['number'];?>
</td>
		</tr>
		<tr>
			<td colspan="1">Postcode en plaats</td>
			<td colspan="3"><?php echo $_smarty_tpl->tpl_vars['klant']->value['zipcode'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['city'];?>
</td>
		</tr>
		<tr>
			<td colspan="1">Rekeningnummer</td>
			<td colspan="3"><?php echo $_smarty_tpl->tpl_vars['klant']->value['bank'];?>
</td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Gegevens incassant</h2>
			</td>
		</tr>
		<tr>
			<td colspan="1">Naam</td>
			<td colspan="3">Branche Channel BV</td>
		</tr>
		<tr>
			<td colspan="1">Adres</td>
			<td colspan="3">Weth. E van Dronkelaarsplein 3</td>
		</tr>
		<tr>
			<td colspan="1">Postcode en plaats</td>
			<td colspan="3">7601 VZ Almelo</td>
		</tr>
		<tr>
			<td colspan="1">Bedrag</td>
			<td colspan="3">&euro; <?php echo $_smarty_tpl->tpl_vars['klant']->value['subscriptionprice'];?>
 incl. BTW</td>
		</tr>
		<tr>
			<td colspan="1">Reden afschrijving</td>
			<td colspan="3">Abonnementskosten <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
TV <?php echo $_smarty_tpl->tpl_vars['klant']->value['subscriptionname'];?>
</td>
		</tr>
		<tr>
			<td colspan="1">Datum afschrijving</td>
			<td colspan="3">Op de eerste dag van ieder volgend kwartaal</td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Ondertekening opdrachtgever</h2>
			</td>
		</tr>
		<tr>
			<td colspan="1">Datum</td>
			<td colspan="3">______________________________________</td>
		</tr>
		<tr>
			<td colspan="1">Plaats</td>
			<td colspan="3">______________________________________</td>
		</tr>
		<tr>
			<td colspan="1" style="padding-top: 30px;">Handtekening</td>
			<td colspan="3" style="padding-top: 30px;">______________________________________</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 20px;">
				<p>Met het ondertekenen van dit machtigingformulier geeft de opdrachtgever toestemming aan 
				Branchel Channel BV om doorlopend bovengenoemd bedrag van zijn/haar rekening te laten afschrijven.</p>
			</td>
		</tr>
	</table>
</div>

<div style="page-break-after: always"></div>

<div class="inhoud">
	<table>
		<tr>
			<td colspan="4">
				<div style="padding: 30px 0px 0px 30px">
					<?php echo $_smarty_tpl->tpl_vars['klant']->value['company'];?>
<br =/>
					T.a.v. <?php echo $_smarty_tpl->tpl_vars['klant']->value['title'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['name'];?>
<br />
					<?php echo $_smarty_tpl->tpl_vars['klant']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['number'];?>
<br />
					<?php echo $_smarty_tpl->tpl_vars['klant']->value['zipcode'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['city'];?>

				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px; text-align: right;">Almelo, <?php echo smarty_modifier_date_format(time(),'%e %B %Y');?>
</td>
		</tr>
		<tr>
			<td colspan="4" style="padding-top: 30px;">
				<h1>Hartelijk welkom bij <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
TV</h1>
				<p>Geachte <?php echo $_smarty_tpl->tpl_vars['klant']->value['title'];?>
 <?php echo $_smarty_tpl->tpl_vars['klant']->value['name'];?>
</p>
				
				<p>Een aantal dagen geleden heeft u zich geregistreerd voor <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
TV en wij willen u ook namens <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
 hartelijk welkom heten bij Branche Channel. Branche Channel levert het ChannelCMS, het online beheersysteem, waarmee u uw eigen RedkenTV kunt onderhouden.</p>
				
				<p>Wilt u meer uitleg over ons ChannelCMS dan verwijzen wij u graag naar onze instructievideo's welke u online kunt bekijken op <b>http://<?php echo mb_strtolower($_smarty_tpl->tpl_vars['klant']->value['groupname'], 'UTF-8');?>
.branchechannel.com/tutorials</b>.</p>
				
				<h2>Uw inloggegevens</h2>
				<p>Om in te loggen in uw persoonlijke omgeving in het ChannelCMS gaat u naar <b>http://admin.branchechannel.com</b> en logt u in met de volgende gegevens:</p>
			</td>
		</tr>
		<tr>
			<td colspan="1">E-mailadres</td>
			<td colspan="3"><?php echo $_smarty_tpl->tpl_vars['klant']->value['emailaddress'];?>
</td>
		</tr>
		<tr>
			<td colspan="1">Wachtwoord</td>
			<td colspan="3"><i>Het wachtwoord heeft u reeds ontvangen in de bevestigings e-mail</i></td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>Heeft u de Branche Channel Player nog niet besteld?</h2>
				<p>Deze player kunt u online bestellen bij onze hardware-partner Multimediacenter via de webshop <b>http://branchechannel.multimediacenter.nl</b>. Deze kunt u gemakkelijk en veilig betalen met iDeal.</p>
			
				<p>Namens <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
 en Branche Channel wensen wij u veel plezier en commercieel succes met <?php echo $_smarty_tpl->tpl_vars['klant']->value['groupname'];?>
TV.</p>
				
				<p style="padding-top: 10px;">Met vriendelijke groet,</p>
				
				<p><img src="http://backend.branchechannel.com/template/images/factuur_signature.jpg" alt="" /></p>
				
				<p>Team Branche Channel</p>
			</td>
		</tr>
	</table>
</div>

</body>
</html><?php }} ?>