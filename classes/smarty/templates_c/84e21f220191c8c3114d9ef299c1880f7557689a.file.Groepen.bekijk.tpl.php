<?php /* Smarty version Smarty-3.1.8, created on 2012-11-23 15:58:05
         compiled from "/var/www/vhosts/branchechannel.com/backend/template/Groepen.bekijk.tpl" */ ?>
<?php /*%%SmartyHeaderCode:168492878650acdedb9daa37-15543500%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '84e21f220191c8c3114d9ef299c1880f7557689a' => 
    array (
      0 => '/var/www/vhosts/branchechannel.com/backend/template/Groepen.bekijk.tpl',
      1 => 1353682684,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168492878650acdedb9daa37-15543500',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_50acdedba379e1_04209886',
  'variables' => 
  array (
    'Session' => 0,
    'Page' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_50acdedba379e1_04209886')) {function content_50acdedba379e1_04209886($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("Main.header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['Session']->value->data!==false){?>

		<h1>Groep: <?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['company'];?>
</h1>
		
		<div class="title">
			<h2>Gegevens</h2> 
			<div class="options">
				<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['id'];?>
/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> 
				<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['id'];?>
/status/"><img src="template/images/icons/<?php if ($_smarty_tpl->tpl_vars['Page']->value->groep['status']){?>accept<?php }else{ ?>delete<?php }?>.png" alt="" title="<?php if ($_smarty_tpl->tpl_vars['Page']->value->groep['status']){?>Ina<?php }else{ ?>A<?php }?>ctief maken" /></a> 
				<a href="groepen/<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['id'];?>
/verwijder/"><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a>
			</div>
		</div>
		<table>
			<tr>
				<th style="width: 20%;">Groepsnaam</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['company'];?>
</th>
			</tr>
			<tr>
				<th>Contactpersoon</th>
				<td><?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['title'];?>
 <?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['name'];?>
</td>
			</tr>
			<tr>
				<th>E-mailadres</th>
				<td><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['emailaddress'];?>
"><?php echo $_smarty_tpl->tpl_vars['Page']->value->groep['emailaddress'];?>
</a></td>
			</tr>
		</table>
		
		
		
		<h2>Abonnementen</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="30%">Abonnement</th>
				<th width="10%">Duur</th>
				<th width="20%">Aangesloten klanten</th>
				<th width="20%">Prijs per kwartaal (ex)</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->abonnementen; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="">1 jarig</a></td>
				<td>1 jaar</td>
				<td>123</td>
				<td>&euro; 34,95</td>
				<td style="text-align: right;"><a href=""><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> <a href=""><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> <a href=""><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a></td>
			</tr>
<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
			<tr>
				<td colspan="6"><em>Er zijn geen abonnementen bij deze groep</em></td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="3"></td>
				<td colspan="3"></td>
			</tr>
		</table>
		


		<h2>Klanten</h2>
		<table class="overview">
			<tr style="border: none;">
				<th width="10%">Klantnummer</th>
				<th width="50%">Bedrijfsnaam</th>
				<th width="20%">Abonnement</th>
				<th width="10%">Channel</th>
				<th width="10%" style="text-align: right;">Opties</th>
			</tr>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['Page']->value->klanten; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<tr>
				<td><a href="klanten/bekijk/">00001</a></td>
				<td>Salon Bas</td>
				<td><a href="">3 jarig</a></td>
				<td><a href="">00001</a></td>
				<td style="text-align: right;"><a href="klanten/bewerk/"><img src="template/images/icons/page_edit.png" alt="" title="Bewerken" /></a> <a href=""><img src="template/images/icons/accept.png" alt="" title="Inactief maken" /></a> <a href=""><img src="template/images/icons/bin_closed.png" alt="" title="Verwijderen" /></a></td>
			</tr>
<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
			<tr>
				<td colspan="6"><em>Er zijn geen klanten bij deze groep</em></td>
			</tr>
<?php } ?>
			<tr>
				<td colspan="3">Toon <select><option>20</option><option>50</option><option>100</option></select> per pagina</td>
				<td colspan="3">1 <a href="">2</a> <a href="">3</a> <a href="">4</a> <a href="">5</a> <a href="">&rsaquo;</a> <a href="">&raquo;</a></td>
			</tr>
		</table>
		
<?php }?>
		
<?php echo $_smarty_tpl->getSubTemplate ("Main.footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>