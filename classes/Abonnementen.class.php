<?php

class Abonnementen {

	public $abonnementen = array();
	public $groepen = array();
	
	public function __construct() {
	
		global $Main;
	
		if(isset($_POST['add'])) {
		
			$this->add();
		
		}
	
		if(is_numeric($Main->sub)) {
		
			if($Main->action == 'status') {
			
				$this->status($Main->sub);
				
			}
			
			if($Main->action == 'verwijder') {
			
				$this->verwijder($Main->sub);
			
			}
			
			if(isset($_POST['edit'])) {
			
				$this->edit($Main->sub);
			
			}
		
			$this->bekijk($Main->sub);
			
			$Main->sub = $Main->action;
		
		}
		
		if($Main->sub == 'nieuw' || $Main->sub == 'bewerk') {
		
			$this->prepare();
			
		}
	
		$this->abonnementen();
	
	}
	
	protected function add() {
	
		global $Db;
		
		$name = htmlspecialchars($_POST['name'], ENT_QUOTES);
		$group = intval($_POST['group']);
		$duration = intval($_POST['duration']);
		$price = floatval(str_replace(',', '.', $_POST['price']));		
		
		$Db->query("INSERT INTO `subscriptions` VALUES (NULL, '".$name."', '".$group."', '".$price."', '".$duration."', 1)");
		
		header('Location: ../');
		exit;
	
	}
	
	protected function status($id) {
	
		global $Db;
		
		list($status) = $Db->query("SELECT `status` FROM `subscriptions` WHERE `id` = '".$id."'")->fetch_row();
		
		$Db->query("UPDATE `subscriptions` SET `status` = '".($status ? 0 : 1)."' WHERE `id` = '".$id."'");
		
		header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
	
	}
	
	protected function verwijder($id) {
	
		global $Db;
		
		$Db->query("DELETE FROM `subscriptions` WHERE `id` = '".$id."'");
		
		header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
	
	}
	
	protected function edit($id) {
	
		global $Db;
		
		$name = htmlspecialchars($_POST['name'], ENT_QUOTES);
		$group = intval($_POST['group']);
		$duration = intval($_POST['duration']);
		$price = floatval(str_replace(',', '.', $_POST['price']));
		
		$Db->query("UPDATE `subscriptions` SET `name` = '".$name."', `group` = '".$group."', `duration` = '".$duration."', `price` = '".$price."' WHERE `id` = '".$id."'");
		
		header('Location: ../');
		exit;
	
	}
	
	protected function bekijk($id) {
	
		global $Db;
	
		$this->abonnement = $Db->query("SELECT * FROM `subscriptions` WHERE `id` = '".$id."'")->fetch_assoc();
	
	}
	
	protected function prepare() {
	
		global $Db;
		
		$result = $Db->query("SELECT * FROM `users` WHERE `type` = 1 ORDER BY `company` ASC");
		
		while($row = $result->fetch_assoc()) {
		
			$this->groepen[] = $row;
			
		}
		
		$result->free();
	
	}
	
	protected function abonnementen() {
	
		global $Db;
		
		$result = $Db->query("SELECT `s`.*, `u`.`company` AS `groupname`, (SELECT COUNT(*) FROM `users` WHERE `subscription` = `s`.`id`) AS `customers` FROM `subscriptions` AS `s` LEFT JOIN `users` AS `u` ON `s`.`group` = `u`.`id` ORDER BY `s`.`name` ASC");
		
		while($row = $result->fetch_assoc()) {
		
			$this->abonnementen[] = $row;
		
		}
				
		$result->free();
	
	}

}

$Page = new Abonnementen;

?>