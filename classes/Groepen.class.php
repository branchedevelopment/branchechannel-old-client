<?php

class Groepen {

	public $groepen = array();
	public $abonnementen = array();
	public $klanten = array();
		
	public function __construct() {
	
		global $Main;
	
		if(isset($_POST['add'])) {
		
			$this->add();
		
		}
	
		if(is_numeric($Main->sub)) {
		
			if($Main->action == 'status') {
			
				$this->status($Main->sub);
				
			}
			
			if($Main->action == 'verwijder') {
			
				$this->verwijder($Main->sub);
			
			}
			
			if(isset($_POST['edit'])) {
			
				$this->edit($Main->sub);
			
			}
		
			$this->bekijk($Main->sub);
			
			$Main->sub = $Main->action;
		
		}
	
		$this->groepen();
	
	}
	
	protected function add() {
	
		global $Db, $Main;
		
		$name = filter_input(INPUT_POST, 'contactpersoon', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $title = filter_input(INPUT_POST,'aanhef', FILTER_SANITIZE_STRING);
		$emailaddress = filter_input(INPUT_POST, 'emailadres', FILTER_SANITIZE_EMAIL);
		$company = filter_input(INPUT_POST, 'bedrijfsnaam', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $password = filter_input(INPUT_POST,'wachtwoord', FILTER_SANITIZE_STRING);
		
        $insertUser = "INSERT INTO `users` (date, hash, name, emailaddress, `group`, subscription, installhelp, company, title, phonenumber, mobilenumber, street, number, zipcode, city, dstreet, dnumber, dzipcode, dcity, bank, bankowner, kvk, btw, password, type, config, status, player)"
                . "VALUES (NOW(), '', '".$name."', '".$emailaddress."', 0, 0, '', '".$company."', '".$_POST['aanhef']."', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '".md5($_POST['wachtwoord'])."', 1, '', 1, 2)";
        
		if($Db->query($insertUser)) {
		
    		$group = $Db->insert_id;
    		
    		$Db->query("UPDATE `users` SET `hash` = '".md5($group)."' WHERE `id` = '".$group."'");
    		$Db->query("INSERT INTO `design` VALUES (NULL, '".$group."', '".$_POST['achtergrondkleur']."', '".$_POST['tekstkleur']."')");
    		
    		if(isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {
    		
    		    if(move_uploaded_file($_FILES['logo']['tmp_name'], CONTENT_SERVER . $_FILES['logo']['name'])) {
    		    
    		        require_once('classes/phpthumb/ThumbLib.inc.php');
    		    
    		        $Thumb = PhpThumbFactory::create(CONTENT_SERVER . $_FILES['logo']['name'], array('resizeUp' => true));
    		        $Thumb->resize(295, 72)->save(CONTENT_SERVER . 'groups/' . sprintf('%05d', $group) . '.png', 'PNG');
    		        
    		        @unlink(CONTENT_SERVER . $_FILES['logo']['name']);
    		    
    		    }
    		
    		}
    			
    		header('Location: ../'.$group.'/bekijk/');
    		exit;
    		
    	}
	
	}
	
	protected function status($id) {
	
		global $Db;
		
		list($status) = $Db->query("SELECT `status` FROM `users` WHERE `id` = '".$id."'")->fetch_row();
		
		$Db->query("UPDATE `users` SET `status` = '".($status ? 0 : 1)."' WHERE `id` = '".$id."'");
		
		header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
	
	}
	
	protected function verwijder($id) {
	
		global $Db;
		
		$Db->query("DELETE FROM `users` WHERE `id` = '".$id."'");
		
		$result = $Db->query("SELECT `id`, `type` FROM `content` WHERE `kid` = '".$id."'");
		
		while(list($id, $type) = $result->fetch_row()) {
		
			if($type == 1) {
				@unlink(CONTENT_SERVER . 'images/' . $id .'.jpg');
				@unlink(CONTENT_SERVER . 'images/' . $id .'_thumb.jpg');
			}
		
			if($type == 3) {
				@unlink(CONTENT_SERVER . 'messages/'.$id.'_picture.jpg');
				@unlink(CONTENT_SERVER . 'messages/'.$id.'_info.jpg');
				@unlink(CONTENT_SERVER . 'messages/'.$id.'_offer.jpg');
				@unlink(CONTENT_SERVER . 'messages/'.$id.'_thumb.jpg');
			}
			
			if($type == 4) {
				@unlink(CONTENT_SERVER . 'library/' . $id . '_picture.jpg');
				@unlink(CONTENT_SERVER . 'library/' . $id . '_info.jpg');
				@unlink(CONTENT_SERVER . 'library/' . $id . '_offer.jpg');
				@unlink(CONTENT_SERVER . 'library/' . $id . '_thumb.jpg');
			}
			
			$Db->query("DELETE FROM `channels` WHERE `cid` = '".$id."'");
		
		}
		
		$Db->query("DELETE FROM `content` WHERE `kid` = '".$id."'");
		
		header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
	
	}	
	
	protected function edit($id) {
	
		global $Db;
		
		$company = htmlspecialchars($_POST['company'], ENT_QUOTES);
		$name = htmlspecialchars($_POST['name'], ENT_QUOTES);
		
		$Db->query("UPDATE `users` SET `company` = '".$company."', `name` = '".$name."', `emailaddress` = '".$_POST['emailaddress']."', `title` = '".$_POST['title']."' WHERE `id` = '".$id."'");
		$Db->query("UPDATE `design` SET `achtergrondkleur` = '".$_POST['achtergrondkleur']."', `tekstkleur` = '".$_POST['tekstkleur']."' WHERE `group` = '".$id."'");
		
		if(isset($_POST['wachtwoord']) && $_POST['wachtwoord'] != '') {
		    $Db->query("UPDATE `users` SET `password` = '".md5($_POST['wachtwoord'])."' WHERE `id` = '".$id."'");
		}
		
		if(isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {
		
		    if(move_uploaded_file($_FILES['logo']['tmp_name'], CONTENT_SERVER . $_FILES['logo']['name'])) {
		    
		    	require_once('classes/phpthumb/ThumbLib.inc.php');
		    
		        $Thumb = PhpThumbFactory::create(CONTENT_SERVER . $_FILES['logo']['name'], array('resizeUp' => true));
		        $Thumb->resize(295, 72)->save(CONTENT_SERVER . 'groups/' . $id . '.png', 'PNG');
		        
		        @unlink(CONTENT_SERVER . $_FILES['logo']['name']);
		    
		    }
		
		}
		
		header('Location: ../bekijk/');
		exit;
	
	}
	
	protected function bekijk($id) {
	
		global $Db;
	
		$this->groep = $Db->query("SELECT `u`.*, `d`.`achtergrondkleur`, `d`.`tekstkleur` FROM `users` AS `u` LEFT JOIN `design` AS `d` ON `u`.`id` = `d`.`group` WHERE `u`.`id` = '".$id."'")->fetch_assoc();
	    $this->groep['logo'] = file_exists(CONTENT_SERVER . 'groups/' . $id . '.png');
	    	
	}
	
	protected function groepen() {
	
		global $Db;
		
		$result = $Db->query("SELECT `u`.*, COUNT(`s`.`id`) AS `subscriptions`, (SELECT COUNT(*) FROM `users` WHERE `group` = `u`.`id`) AS `customers` FROM `users` AS `u` LEFT JOIN `subscriptions` AS `s` ON `u`.`id` = `s`.`group` WHERE `u`.`type` = 1 GROUP BY `u`.`id` ORDER BY `u`.`name` ASC");
		
		while($row = $result->fetch_assoc()) {
		
			$this->groepen[] = $row;
		
		}
				
		$result->free();
	
	}

}

$Page = new Groepen;

?>