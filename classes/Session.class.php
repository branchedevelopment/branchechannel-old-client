<?php

if(!defined('INDEX')) exit;

class Session {

	public $name = 'bc';
	public $data = false;

	// Constructor
	public function __construct() {
	
		if(!isset($_COOKIE[$this->name])) {
			session_start();
		}
		
		if(isset($_GET['op']) && $_GET['op'] == 'uitloggen') {
		
			$this->delete();
			return;
			
		}
		
		if(isset($_POST['forgot'])) {
		
			$this->forgotten($_POST['emailadres']);
			
		}
	
		if(isset($_POST['login'])) {
						
			$this->create($_POST['emailadres'], $_POST['wachtwoord'], isset($_POST['remember']));
			return;
			
		}
			
		if(isset($_SESSION[$this->name]) || isset($_COOKIE[$this->name])) {
		
			$this->validate();
			
		}
	
	}
	
	protected function check($emailadres, $wachtwoord) {

		global $Db;
		
		$result = $Db->query("SELECT * FROM `admin` WHERE `emailadres`='".$emailadres."' AND `wachtwoord`='".$wachtwoord."'");
		
		return $result;	
		
	}
	
	protected function delete() {
		
		if(isset($_SESSION[$this->name])) unset($_SESSION[$this->name]);
		
		if(isset($_COOKIE[$this->name])) {
			unset($_COOKIE[$this->name]);
			setcookie($this->name, false, 1, '/', 'backend.branchechannel.com', 0);
		}
		
		header('Location: '.URL);
		exit;
	
	}
	
	protected function forgotten($emailadres) {
	
		global $Db, $Smarty, $Mail;
		
		$result = $Db->query("SELECT `name` FROM `admin` WHERE `emailadres` = '".$emailadres."'");
		
		if($result->num_rows) {
		
			$wachtwoord = $this->wachtwoord();
		
			$Db->query("UPDATE `admin` SET `wachtwoord` = '".md5($wachtwoord)."' WHERE `emailadres` = '".$emailadres."'");
		
			list($naam) = $result->fetch_row();
	
			$Smarty->assign(array(
				'naam' => $naam,
				'email' => $emailadres,
				'wachtwoord' => $wachtwoord
			));
							
			$content = $Smarty->fetch('Login.mail.tpl');
			
			$Mail->Subject = "Uw nieuwe wachtwoord";
			$Mail->MsgHTML($content);
			$Mail->AddAddress($emailadres, $naam);
			$Mail->Send();	
		
		}
		
		header('Location: '.URL);
		exit;
	
	}
	
	protected function validate() {
	
		if(isset($_SESSION[$this->name])) {
	
			list($emailadres, $wachtwoord) = explode(':', base64_decode($_SESSION[$this->name]));
			
		} else {
		
			list($emailadres, $wachtwoord) = explode(':', base64_decode($_COOKIE[$this->name]));		
		
		}
	
		$result = $this->check($emailadres, $wachtwoord);
	
		if($result->num_rows) {
		
			$this->data = $result->fetch_assoc();
			
		}
		
	}
		
	
	protected function create($emailadres, $wachtwoord, $persistent) {
	
		$wachtwoord = md5($wachtwoord);
	
		$result = $this->check($emailadres, $wachtwoord);
		
		if($result->num_rows) {
		
			if($persistent) {
			
				setcookie($this->name, base64_encode($emailadres.':'.$wachtwoord), pow(2,31)-1, '/', 'backend.branchechannel.com', 0);
				
			} else {
		
				$_SESSION[$this->name] = base64_encode($emailadres.':'.$wachtwoord);
				
			}
			
			$this->data = $result->fetch_assoc();
		
		}
		
		header('Location: '.URL);
		exit;
		
	}
	
	public function wachtwoord() {
		
		$chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
		srand((double)microtime()*1000000); 
		
		$pass = ''; 
		
		for($i=0;$i<=7;$i++) {
			$num = rand() % 33; 
			$tmp = substr($chars, $num, 1); 
			$pass = $pass . $tmp; 
		}
		
		return $pass;
		
	}

}

$Session = new Session;

?>