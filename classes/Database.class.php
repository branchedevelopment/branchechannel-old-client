<?php

if(!defined('INDEX')) exit;

class Database extends MySQLi {
	
	public $query_count = 0;

  	public function query($query, $resultmode = MYSQLI_STORE_RESULT) {
		
    	$this->query_count++;
		
    	return parent::query($query, $resultmode);
		
  	}	
	
}

try {

	$Db = @new Database(DBHOST, DBUSER, DBPASS, DBNAME);
	
	if($Db->connect_error) {
		throw new Exception($Db->connect_error, $Db->connect_errno);
	}
	
} catch(Exception $e) {

	exit($e->getMessage());
	
}

?>