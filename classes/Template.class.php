<?php

if(!defined('INDEX')) exit;

// Smarty inladen
require_once(BASEURL.'/classes/smarty/Smarty.class.php');

// Smarty initaliseren
$Smarty = new Smarty;

// Bepalen of er gecached mag worden en cache instellen
//$Smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);

// Error reporting instellen
$Smarty->error_reporting = E_ALL & ~E_NOTICE;

// Paden instellen
$Smarty->setTemplateDir(BASEURL.'/template');
$Smarty->setCompileDir(BASEURL.'/classes/smarty/templates_c');
$Smarty->setCacheDir(BASEURL.'/classes/smarty/cache');

?>