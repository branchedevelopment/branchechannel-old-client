<?php

class Klanten {

	public $klanten = array();
	public $groepen = array();
	public $abonnementen = array();
	
	public $facturen = array();
	public $tickets = array();
	
	public function __construct() {
	
		global $Main;
	
		if(is_numeric($Main->sub)) {
		
			if($Main->action == 'status') {
			
				$this->status($Main->sub);
			
			}
			
			if($Main->action == 'verwijder') {
			
				$this->verwijder($Main->sub);
				
			}
			
			if(isset($_POST['edit'])) {
			
				$this->edit($Main->sub);
			
			}
		
			$this->bekijk($Main->sub);
			
			$Main->sub = $Main->action;
		
		}
	
		$this->klanten();
	
	}
	
	protected function status($id) {
	
		global $Db;
		
		list($status) = $Db->query("SELECT `status` FROM `users` WHERE `id` = '".$id."'")->fetch_row();
		
		$Db->query("UPDATE `users` SET `status` = '".($status ? 0 : 1)."' WHERE `id` = '".$id."'");
		
		header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
	
	}
	
	protected function verwijder($id) {
	
		global $Db;
		
		$Db->query("DELETE FROM `users` WHERE `id` = '".$id."'");
		$Db->query("DELETE FROM `channels` WHERE `gid` = '".$id."'");
		$Db->query("DELETE FROM `invoices` WHERE `user` = '".$id."'");
		$Db->query("DELETE FROM `rss` WHERE `kid` = '".$id."'");
		
		$result = $Db->query("SELECT `id`, `type` FROM `content` WHERE `kid` = '".$id."'");
		
		while(list($id, $type) = $result->fetch_row()) {
		
			if($type == 1) {
				@unlink('../../content.branchechannel.com/html/images/'.$id.'.jpg');
				@unlink('../../content.branchechannel.com/html/images/'.$id.'_thumb.jpg');
			}
		
			if($type == 3) {
				@unlink('../../content.branchechannel.com/html/messages/'.$id.'_picture.jpg');
				@unlink('../../content.branchechannel.com/html/messages/'.$id.'_info.jpg');
				@unlink('../../content.branchechannel.com/html/messages/'.$id.'_offer.jpg');
				@unlink('../../content.branchechannel.com/html/messages/'.$id.'_thumb.jpg');
			}
			
			if($type == 4) {
				@unlink('../../content.branchechannel.com/html/library/'.$id.'_picture.jpg');
				@unlink('../../content.branchechannel.com/html/library/'.$id.'_info.jpg');
				@unlink('../../content.branchechannel.com/html/library/'.$id.'_offer.jpg');
				@unlink('../../content.branchechannel.com/html/library/'.$id.'_thumb.jpg');
			}
		
		}
		
		$Db->query("DELETE FROM `content` WHERE `kid` = '".$id."'");
		
		header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
	
	}
	
	protected function edit($id) {
	
		global $Db;
		
		$name = htmlspecialchars($_POST['contactpersoon'], ENT_QUOTES);
		$emailaddress = $_POST['emailadres'];
		$company = htmlspecialchars($_POST['bedrijfsnaam'], ENT_QUOTES);
		$street = htmlspecialchars($_POST['straatnaam'], ENT_QUOTES);
		$number = htmlspecialchars($_POST['huisnummer'], ENT_QUOTES);
		$zipcode = htmlspecialchars($_POST['postcode'], ENT_QUOTES);
		$city = htmlspecialchars($_POST['woonplaats'], ENT_QUOTES);
		$bank = htmlspecialchars($_POST['bank'], ENT_QUOTES);
		$kvk = htmlspecialchars($_POST['kvk'], ENT_QUOTES);
		$btw = htmlspecialchars($_POST['btw'], ENT_QUOTES);
		
		$Db->query("UPDATE `users` SET `name` = '".$name."', `emailaddress` = '".$emailaddress."', `group` = '".intval($_POST['group'])."', `subscription` = '".intval($_POST['subscription'])."', `company` = '".$company."', `title` = '".$_POST['aanhef']."', `street` = '".$street."', `number` = '".$number."', `zipcode` = '".$zipcode."', `city` = '".$city."', `bank` = '".$bank."', `kvk` = '".$kvk."', `btw` = '".$btw."' WHERE `id` = '".$id."'");
		
		header('Location: ../bekijk/');
		exit;	
	
	}
	
	protected function bekijk($id) {
	
		global $Db;
	
		$this->klant = $Db->query("SELECT `u`.*, `g`.`company` AS `groupname`, `s`.`name` AS `subscriptionname` FROM `users` AS `u` LEFT JOIN `users` AS `g` ON `u`.`group` = `g`.`id` LEFT JOIN `subscriptions` AS `s` ON `u`.`subscription` = `s`.`id` WHERE `u`.`id` = '".$id."'")->fetch_assoc();
		
		$result = $Db->query("SELECT * FROM `users` WHERE `type` = 1 ORDER BY `company` ASC");
		
		while($row = $result->fetch_assoc()) {
		
			$this->groepen[] = $row;
			
		}
		
		$result->free();
		
		$result = $Db->query("SELECT * FROM `subscriptions` WHERE `group` = '".$this->klant['group']."' ORDER BY `name` ASC");
		
		while($row = $result->fetch_assoc()) {
		
			$this->abonnementen[] = $row;
			
		}
		
		$result->free();
	
	}
	
	protected function klanten() {
	
		global $Db;
		
		$result = $Db->query("SELECT `u`.*, `g`.`company` AS `groupname`, `s`.`name` AS `subscriptionname` FROM `users` AS `u` LEFT JOIN `users` AS `g` ON `u`.`group` = `g`.`id` LEFT JOIN `subscriptions` AS `s` ON `u`.`subscription` = `s`.`id` WHERE `u`.`type` = 0 ORDER BY `u`.`id` ASC");
		
		while($row = $result->fetch_assoc()) {
		
			$this->klanten[] = $row;
		
		}
				
		$result->free();
	
	}

}

$Page = new Klanten;

?>