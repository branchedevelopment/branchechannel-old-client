<?php

class Facturen {

	public $factuur;
	public $facturen = array();
		
	public function __construct() {
	
		global $Main;
	
		if(is_numeric($Main->sub)) {
		
			$this->bekijk($Main->sub);
			
			$Main->sub = $Main->action;
		
		}
		
		//$this->create();
	
		$this->facturen();
	
	}
	
	protected function create() {
	
		$result = $Db->query("SELECT * FROM `users` WHERE `type` = 0");
		
		while($row = $result->fetch_assoc()) {
		
			$Db->query("INSERT INTO `invoices` VALUES (NULL, NOW(), '".$id."')");
		
		}
		
		$result->free();
	
	}
	
	protected function bekijk($id) {
	
		global $Db, $Smarty;
	
		$this->factuur = $Db->query("SELECT `i`.`id` AS `fid`, `i`.`date` AS `fdate`, `u`.*, `s`.`name` AS `subscription`, `s`.`price` FROM `invoices` AS `i` LEFT JOIN `users` AS `u` ON `i`.`user` = `u`.`id` LEFT JOIN `subscriptions` AS `s` ON `u`.`subscription` = `s`.`id` WHERE `i`.`id` = '".$id."'")->fetch_assoc();
		
		$this->factuur['todate'] = strtotime($this->factuur['todate'].' +3 months -1 day');
		
		require_once('classes/dompdf/dompdf_config.inc.php');
	
		$Smarty->assign('Page', $this);
		$content = $Smarty->fetch('Facturen.factuur.tpl');
				
		$dompdf = new DOMPDF();
		$dompdf->load_html($content);
		$dompdf->set_paper('a4', 'portrait');
		$dompdf->render();
		$dompdf->stream(date('Y', strtotime($this->factuur['fdate']))."-".$this->factuur['id']."-".$this->factuur['fid'].".pdf");	
		
		exit;
	
	}
	
	protected function facturen() {
	
		global $Db;
		
		$result = $Db->query("SELECT `i`.`id` AS `fid`, `i`.`date` AS `fdate`, `u`.`id` AS `klantnr`, `u`.`company`, `s`.`name` AS `subscription` FROM `invoices` AS `i` LEFT JOIN `users` AS `u` ON `i`.`user` = `u`.`id` LEFT JOIN `subscriptions` AS `s` ON `u`.`subscription` = `s`.`id` ORDER BY `i`.`id` DESC");
		
		while($row = $result->fetch_assoc()) {
		
			$this->facturen[] = $row;
		
		}
				
		$result->free();
	
	}

}

$Page = new Facturen;

?>