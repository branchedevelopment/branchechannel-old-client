<?php

if(!defined('INDEX')) exit;

class Main {
	
	public $op;
	public $sub;
	public $action;
	public $other;
		
	public function __construct() {
						
		$this->op = (isset($_GET['op']) && file_exists('template/'.ucfirst($_GET['op']).'.index.tpl')) ? $_GET['op'] : 'dashboard';
		$this->sub = isset($_GET['sub']) ? $_GET['sub'] : 'index';
		$this->action = isset($_GET['action']) ? $_GET['action'] : 'index';
		$this->other = isset($_GET['other']) ? $_GET['other'] : 'index';
		
	}
	
	public function urlprepare($string) {
	
	    $string = strtolower($string);
	    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	    $string = preg_replace("/[\s-]+/", " ", $string);
	    $string = preg_replace("/[\s_]/", "-", $string);
	    
	    return $string;
		
	}
	
	public function password() {
		
		$chars = "abcdefghijkmnopqrstuvwxyz023456789"; 		
		$pass = ''; 
		
		for($i=0;$i<=7;$i++) {
			$num = mt_rand() % 33; 
			$tmp = substr($chars, $num, 1); 
			$pass .= $tmp; 
		}
		
		return $pass;
		
	}
		
}

$Main = new Main;

?>