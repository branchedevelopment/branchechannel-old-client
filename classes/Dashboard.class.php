<?php

class Dashboard {

	public $aanmeldingen = array();
	public $tickets = array();
	
	public function __construct() {
	
		global $Main;
		
		if(isset($_POST['status'])) {
		
			$this->status($_POST['id'], $_POST['status']);
		
		}
		
		if(is_numeric($Main->sub)) {
			
			if($Main->action == 'brief') {
			
				$this->brief($Main->sub);
				
			}
		
		}
	
		$this->aanmeldingen();
		$this->tickets();
	
	}
	
	protected function status($id, $status) {
	
		global $Db;
		
		$Db->query("UPDATE `users` SET `player` = '".$status."' WHERE `id` = '".$id."'");
		
		header('Location: '.$_SERVER['HTTP_REFERER']);
		exit;
	
	}
	
	protected function brief($id) {
	
		global $Db, $Smarty;
		
		$this->klant = $Db->query("SELECT `u`.*, `g`.`company` AS `groupname`, `s`.`name` AS `subscriptionname`, `s`.`price` FROM `users` AS `u` LEFT JOIN `users` AS `g` ON `u`.`group` = `g`.`id` LEFT JOIN `subscriptions` AS `s` ON `u`.`subscription` = `s`.`id` WHERE `u`.`id` = '".$id."'")->fetch_assoc();
		
		$this->klant['subscriptionprice'] = number_format(($this->klant['price']*3)*1.21, 2, ',', '.');
				
		require_once('classes/dompdf/dompdf_config.inc.php');
	
		$Smarty->assign('klant', $this->klant);
		$content = $Smarty->fetch('Dashboard.brief.tpl');
				
		$dompdf = new DOMPDF();
		$dompdf->load_html($content);
		$dompdf->set_paper('a4', 'portrait');
		$dompdf->render();
		$dompdf->stream("welkomsbrief.pdf");
		
		exit;
	
	}
	
	protected function aanmeldingen() {
	
		global $Db;
		
		$result = $Db->query("SELECT `u`.*, `g`.`company` AS `groupname`, `s`.`name` AS `subscriptionname` FROM `users` AS `u` LEFT JOIN `users` AS `g` ON `u`.`group` = `g`.`id` LEFT JOIN `subscriptions` AS `s` ON `u`.`subscription` = `s`.`id` WHERE `u`.`player` < 2 AND `u`.`type` = 0 ORDER BY `u`.`id` ASC");
		
		while($row = $result->fetch_assoc()) {
		
			$this->aanmeldingen[] = $row;
		
		}
				
		$result->free();
	
	}
	
	protected function tickets() {
	
	
	
	}

}

$Page = new Dashboard;

?>